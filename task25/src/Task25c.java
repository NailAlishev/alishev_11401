import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

/**
 * @author Nail Alishev
 *         11-401
 *         task 25c
 */
public class Task25c {
    public static void main(String[] args) {
        printMap(authorsByName());
        System.out.println();
        printMap(authorsByParameters());
        System.out.println();
        printMap(authorsByVoidMethods());
        System.out.println();
        printMap(authorsByParametersNumber());

    }

    public static List<Method> getAllMethodsFromAllClasses() {
        List<Method> list = new ArrayList<Method>();
        list.addAll(Arrays.asList(ComplexNumber.class.getDeclaredMethods()));
        list.addAll(Arrays.asList(RationalFraction.class.getDeclaredMethods()));
        list.addAll(Arrays.asList(Vector2D.class.getDeclaredMethods()));
        return list;
    }

    public static Map<String, Double> authorsByName() {
        List<Method> list = getAllMethodsFromAllClasses();
        HashMap<String, Double> results = new HashMap<String, Double>();
        HashMap<String, Integer> numOfMethods = new HashMap<String, Integer>();
        for (Method m : list) {
            String author = m.getAnnotation(annotations.author.class).name();
            if (results.containsKey(author)) {
                results.put(author, results.get(author) + m.getName().length());
                numOfMethods.put(author, numOfMethods.get(author) + 1);
            } else {
                results.put(author, (double) m.getName().length());
                numOfMethods.put(author, 1);
            }
        }
        for (String author : results.keySet()) {
            results.put(author, results.get(author) / numOfMethods.get(author));
        }
        return sortMap(results);
    }

    public static Map<String, Double> authorsByParameters() {
        List<Method> list = getAllMethodsFromAllClasses();
        HashMap<String, Double> results = new HashMap<String, Double>();
        HashMap<String, Integer> numOfParams = new HashMap<String, Integer>();
        for (Method m : list) {
            String author = m.getAnnotation(annotations.author.class).name();
            if (!results.containsKey(author)) {
                results.put(author, 0.0);
                numOfParams.put(author, 0);
            }
            for (Parameter p : m.getParameters()) {
                results.put(author, results.get(author) + p.toString().length());
                //не могу получить доступ к именам параметров, гугл говорит, что имена недоступны в runtime
                //вместо моих имен параметров есть только имена типа arg0, arg1  и.т.д
                numOfParams.put(author, numOfParams.get(author) + 1);
            }
        }
        for (String author : results.keySet()) {
            results.put(author, results.get(author) / numOfParams.get(author));
        }
        return sortMap(results);
    }

    public static Map<String, Double> authorsByVoidMethods() {
        List<Method> list = getAllMethodsFromAllClasses();
        HashMap<String, Double> results = new HashMap<String, Double>();
        for (Method m : list) {
            String author = m.getAnnotation(annotations.author.class).name();
            if (!results.containsKey(author)) {
                results.put(author, 0.0);
            }
            if (m.getReturnType().toString().equals("void"))
                results.put(author, results.get(author) + 1);
        }
        return sortMap(results);
    }

    public static Map<String, Double> authorsByParametersNumber() {
        List<Method> list = getAllMethodsFromAllClasses();
        HashMap<String, Double> results = new HashMap<String, Double>();
        for (Method m : list) {
            String author = m.getAnnotation(annotations.author.class).name();
            if (!results.containsKey(author)) {
                results.put(author, 0.0);
            }
            for (Parameter p : m.getParameters()) {
                results.put(author, results.get(author) + 1);
            }
        }

        return sortMap(results);
    }


    private static Map<String, Double> sortMap(Map<String, Double> map2BeSorted) {
        List<Map.Entry<String, Double>> sortingList = new ArrayList<Map.Entry<String, Double>>(map2BeSorted.entrySet());
        Collections.sort(sortingList, new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                if (o1.getValue() > o2.getValue()) {
                    return 1;
                } else if (o1.getValue() < o2.getValue()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        LinkedHashMap<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : sortingList) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    private static void printMap(Map<String, Double> map) {
        for (String author : map.keySet()) {
            System.out.printf("%s : %.2f\n", author, map.get(author));
        }
    }
}
