import annotations.author;

/**
 * @author Nail Alishev
 *         11-401
 *         task 25c
 */
public class RationalFraction {
    private int a, b;

    public RationalFraction() {
        this(0, 0);
    }


    public RationalFraction(int a, int b) {
        this.a = a;
        this.b = b;

    }

    @author(name = "Edik")
    public void reduce() {
        int a1 = a;
        int b1 = b;
        while (b1 != 0) {
            int tmp = a1 % b1;
            a1 = b1;
            b1 = tmp;
        }
        a = a / a1;
        b = b / a1;
    }

    @author(name = "Leo")
    public void setA(int a) {
        this.a = a;
    }

    @author(name = "Nikita")
    public void setB(int b) {
        this.b = b;
    }

    @author(name = "Katya")
    public int getA() {
        return a;
    }

    @author(name = "Rustam")
    public int getB() {
        return b;
    }

    @author(name = "Edik")
    public RationalFraction add(RationalFraction ratio) {
        int x, y;


        RationalFraction rational = new RationalFraction();

        if (this.b == ratio.getB()) {
            x = this.a + ratio.getA();
            rational.setA(x);
            rational.setB(this.b);
            rational.reduce();


        } else {
            x = this.a * ratio.getB() + ratio.getA() * this.b;
            y = this.b * ratio.getB();

            rational.setA(x);
            rational.setB(y);
            rational.reduce();

        }
        return rational;
    }

    @author(name = "Leo")
    public void add2(RationalFraction ratio) {
        if (this.b == ratio.getB()) {
            a = a + ratio.getA();

        } else {
            a = this.a * ratio.getB() + ratio.getA() * this.b;
            b = this.b * ratio.getB();
        }
    }

    @author(name = "Rustam")
    public RationalFraction sub(RationalFraction ratio) {
        int x, y;
        RationalFraction rational = new RationalFraction();

        if (this.b == ratio.getB()) {
            x = this.a - ratio.getA();
            rational.setA(x);
            rational.setB(this.b);
            rational.reduce();


        } else {
            x = this.a * ratio.getB() - ratio.getA() * this.b;
            y = this.b * ratio.getB();

            rational.setA(x);
            rational.setB(y);
            rational.reduce();

        }
        return rational;
    }

    @author(name = "Nikita")
    public void sub2(RationalFraction ratio) {
        if (this.b == ratio.getB()) {
            a = a - ratio.getA();

        } else {
            a = this.a * ratio.getB() - ratio.getA() * this.b;
            b = this.b * ratio.getB();
        }
    }

    @author(name = "Katya")
    public RationalFraction mult(RationalFraction ratio) {
        int x, y;
        RationalFraction rational = new RationalFraction();
        x = this.a * ratio.getA();
        y = this.b * ratio.getB();
        rational.setA(x);
        rational.setB(y);
        rational.reduce();
        return rational;
    }

    @author(name = "Edik")
    public void mult2(RationalFraction ratio) {
        a = a * ratio.getA();
        b = b * ratio.getB();

    }

    @author(name = "Edik")
    public RationalFraction div(RationalFraction ratio) {
        int x, y;
        RationalFraction rational = new RationalFraction();
        int d = ratio.getB();
        int d1 = ratio.getA();

        ratio.setA(d);
        ratio.setB(d1);
        x = this.a * ratio.getA();
        y = this.b * ratio.getB();
        rational.setA(x);
        rational.setB(y);
        rational.reduce();
        return rational;
    }

    @author(name = "Katya")
    public void div2(RationalFraction ratio) {
        a = a * ratio.getA();
        b = b * ratio.getB();
    }

    @author(name = "Nikita")
    public String toString() {
        if ((a != b) && b != 1) {
            return a + "/" + b;
        } else if (b == 1) {
            return a + "";
        } else {
            return "1";
        }
    }

    @author(name = "Leo")
    public double value() {
        double x;
        x = (double) a / b;
        return x;
    }

    @author(name = "Rustam")
    public boolean equals(RationalFraction ratio) {
        boolean pr = false;
        if (a == ratio.getA() && b == ratio.getB()) {
            pr = true;
        }
        return pr;
    }

    @author(name = "Edik")
    public int numberPart() {
        int x;
        x = a / b;
        return x;
    }

}
