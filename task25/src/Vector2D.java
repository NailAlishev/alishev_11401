import annotations.author;

/**
 * @author Nail Alishev
 *         11-401
 *         task 25c
 */
public class Vector2D {

    private double a, b;

    public Vector2D() {
        this(0, 0);
    }

    public Vector2D(double a, double b) {
        setA(a);
        setB(b);
    }

    @author(name = "Leo")
    public void setA(double a) {
        this.a = a;
    }

    @author(name = "Katya")
    public void setB(double b) {
        this.b = b;
    }

    @author(name = "Nikita")
    public double getA() {
        return a;
    }

    @author(name = "Edik")
    public double getB() {
        return b;
    }

    @author(name = "Rustam")
    public void print() {
        System.out.print("(" + a + ",");
        System.out.print(b + ")");
        System.out.println();
    }

    @author(name = "Edik")
    public Vector2D add(Vector2D vector) {
        Vector2D векторсложение = new Vector2D();
        double a, b;
        a = this.a + vector.getA();
        b = this.b + vector.getB();
        векторсложение.setA(a);
        векторсложение.setB(b);
        return векторсложение;
    }

    @author(name = "Rustam")
    public void add2(Vector2D vector) {
        a = a + vector.getA();
        b = b + vector.getB();

    }

    @author(name = "Rustam")
    public Vector2D sub(Vector2D vector) {
        Vector2D векторвычитание = new Vector2D();
        double a, b;
        a = this.a - vector.getA();
        b = this.b - vector.getB();
        векторвычитание.setA(a);
        векторвычитание.setB(b);
        return векторвычитание;
    }

    @author(name = "Leo")
    public void sub2(Vector2D vector) {
        a = a - vector.getA();
        b = b - vector.getB();
    }

    @author(name = "Katya")
    public Vector2D mult(double d) {
        Vector2D умножение = new Vector2D();
        double a, b;
        a = this.a * d;
        b = this.b * d;
        умножение.setA(a);
        умножение.setB(b);
        return умножение;
    }

    @author(name = "Leo")
    public void mult2(double d) {
        a = a * d;
        b = b * d;
    }

    @author(name = "Nikita")
    public String toString() {
        return String.format("%f,%f", a, b);
    }

    @author(name = "Rustam")
    public double Length() {

        double d = a * a + b * b;
        d = Math.sqrt(d);
        return d;
    }

    @author(name = "Nikita")
    public double scalarProduct(Vector2D vector) {
        double d = a * vector.getA() + b * vector.getB();
        return d;
    }

    @author(name = "Leo")
    public double cos(Vector2D vector) {
        double d;
        d = scalarProduct(vector) / (Length() * vector.Length());
        return d;
    }

    @author(name = "Edik")
    public boolean equals(Vector2D vector) {
        boolean pr = false;
        if (a == vector.getA() && b == vector.getB()) {
            pr = true;
        }
        return pr;
    }
}