import annotations.author;

/**
 * @author Nail Alishev
 *         11-401
 *         task 25c
 */
public class ComplexNumber {

    private double real, im;


    public ComplexNumber() {
        this(0, 0);
    }

    public ComplexNumber(double real, double im) {
        this.real = real;
        this.im = im;
    }

    @author(name = "Leo")
    public void setReal(double real) {
        this.real = real;
    }

    @author(name = "Edik")
    public void setIm(double im) {
        this.im = im;
    }

    @author(name = "Leo")
    public double getReal() {
        return real;
    }

    @author(name = "Nikita")
    public double getIm() {
        return im;
    }

    @author(name = "Rustam")
    public ComplexNumber add(ComplexNumber b) {
        ComplexNumber c = new ComplexNumber();

        c.setReal(real + b.getReal());
        c.setIm(im + b.getIm());
        return c;

    }

    @author(name = "Leo")
    public void add2(ComplexNumber b) {
        real = this.real + b.getReal();
        im = this.im + b.getIm();
    }

    @author(name = "Nikita")
    public ComplexNumber sub(ComplexNumber b) {
        ComplexNumber c = new ComplexNumber();

        c.setReal(real - b.getReal());
        c.setIm(im - b.getIm());
        return c;
    }

    @author(name = "Katya")
    public void sub2(ComplexNumber b) {
        real = this.real - b.getReal();
        im = this.im - b.getIm();
    }

    @author(name = "Katya")
    public ComplexNumber multNumber(double x) {
        ComplexNumber c = new ComplexNumber();
        c.setReal(this.real * x);
        c.setIm(this.im * x);
        return c;
    }

    @author(name = "Leo")
    public void multNumber2(double x) {
        real = this.real * x;
        im = this.im * x;

    }

    @author(name = "Edik")
    public ComplexNumber mult(ComplexNumber b) {
        ComplexNumber c = new ComplexNumber();
        c.setReal(real * b.getReal() - im * b.getIm());
        c.setIm(real * b.getIm() + im * b.getReal());
        return c;
    }

    @author(name = "Katya")
    public void mult2(ComplexNumber b) {
        double real1 = real;
        real = real * b.getReal() - im * b.getIm();
        im = real1 * b.getIm() + im * b.getReal();

    }

    @author(name = "Leo")
    public ComplexNumber div(ComplexNumber b) {
        ComplexNumber c = new ComplexNumber();
        b.setReal(b.getReal());
        b.setIm(-b.getIm());
        c = mult(b);
        double k = b.length() * b.length();
        c.setReal(c.getReal() / k);
        c.setIm(c.getIm() / k);
        return c;

    }

    @author(name = "Rustam")
    public void div2(ComplexNumber b) {
        ComplexNumber c = new ComplexNumber();
        b.setReal(b.getReal());
        b.setIm(-b.getIm());
        c = mult(b);
        double k = b.length() * b.length();
        real = c.getReal() / k;
        im = c.getIm() / k;


    }

    @author(name = "Nikita")
    public double length() {
        double r = Math.sqrt(getReal() * getReal() + getIm() * getIm());
        return r;
    }

    @author(name = "Katya")
    public String toString() {
        if (im > 0) {
            return (getReal() + "+" + getIm() + "i");
        } else if (im == 0) {
            return (getReal() + "");
        } else
            return (getReal() + "" + getIm() + "i");
    }

    @author(name = "Katya")
    public double arg() {
        return Math.atan2(im, real);

    }

    @author(name = "Katya")
    public ComplexNumber pow(double x) {

        double a, b, c;
        ComplexNumber com = new ComplexNumber();
        com.setIm(this.im);
        com.setReal(this.real);

        a = Math.pow(com.length(), x);
        b = Math.cos(x * com.arg());
        c = Math.sin(x * com.arg());
        com.setReal(a * b);
        com.setIm(a * c);

        return com;


    }

    @author(name = "Edik")
    public boolean equals(ComplexNumber c) {
        boolean pr = false;
        if (this.real == c.getReal() && this.im == c.getIm()) {
            pr = true;
        }
        return pr;
    }

}
