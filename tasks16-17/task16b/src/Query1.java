import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by neil on 18.10.15.
 */
@WebServlet(name = "Query1")
public class Query1 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        Template tmpl = cfg.getTemplate("query1.ftl");
        HashMap<String, Object> root = new HashMap<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                    ""
            );
            PreparedStatement stmt = conn.prepareStatement("SELECT students.name AS student_name, classes.name AS class_name, \"year\" " +
                    "FROM students INNER JOIN attendance ON students.id = attendance.student_id" +
                    " INNER JOIN classes ON classes.id = attendance.class_id " +
                    "WHERE attendance.year>=? AND attendance.year<=? ");
            stmt.setInt(1, 1992);
            stmt.setInt(2, 1998);
            ResultSet rs = stmt.executeQuery();
            ArrayList<Info> rows = new ArrayList<>();
            while (rs.next()) {
                rows.add(new Info(rs.getString("student_name"), rs.getString("class_name"), Integer.toString(rs.getInt("year"))));
            }
            root.put("rows", rows);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
