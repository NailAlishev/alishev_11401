import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletContext;

/**
 * Created by neil on 18.10.15.
 */
public class ConfigSingleton {
    private static Configuration cfg;

    public static Configuration getCfg(ServletContext sc) {
        if(cfg==null){
            cfg = new Configuration(Configuration.VERSION_2_3_22);
            cfg.setServletContextForTemplateLoading(sc,"/WEB-INF/Templates");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        }
        return cfg;
    }
}
