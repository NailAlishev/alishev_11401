<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
<table style ="border: 3px thin solid; border-collapse: collapse; width:100%">
    <style>
        td{
            padding:5px; border: 1px solid black; empty-cells: hide;}
    </style>
<#list rows as row>
    <tr>
        <td>${row.student_name}</td>
        <td>${row.year}</td>
    </tr>
</#list>
</table>
</body>
</html>