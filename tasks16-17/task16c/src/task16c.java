import java.sql.*;

/**
 * @author Nail Alishev
 *         11401
 *         task 16c
 */
public class task16c {
    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                ""
        );
        PreparedStatement stmt = conn.prepareStatement("SELECT students.name AS student_name, classes.name AS class_name, \"year\" " +
                "FROM students INNER JOIN attendance ON students.id = attendance.student_id" +
                " INNER JOIN classes ON classes.id = attendance.class_id " +
                "WHERE attendance.year>=? AND attendance.year<=? ");
        stmt.setInt(1,1992);
        stmt.setInt(2, 1998);
        ResultSet rs = stmt.executeQuery();
        System.out.println("1)student - class - year");
        System.out.println();
        while (rs.next()) {
            System.out.println(rs.getString("student_name")+" - "+rs.getString("class_name")+" - "+rs.getInt("year"));
        }

        stmt = conn.prepareStatement("SELECT students.name AS student_name, \"year\" " +
                "FROM students INNER JOIN attendance " +
                "ON students.id = attendance.student_id " +
                "WHERE attendance.teacher_id = ?");
        stmt.setInt(1,2);
        rs = stmt.executeQuery();
        System.out.println();
        System.out.println("2)student - year");
        System.out.println();
        while(rs.next()){
            System.out.println(rs.getString("student_name")+" - "+rs.getInt("year"));
        }
        stmt.close();
    }
}
