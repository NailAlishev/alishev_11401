import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by neil on 20.10.15.
 */
@WebServlet(name = "Suggest")
public class Suggest extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String category = request.getParameter("category");
        String description = request.getParameter("description");
        if(isValid(name, category, description)){
            PlacesRepository.newPlace(name,description,category);
        }
        response.sendRedirect("/welcome");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        getServletContext().getRequestDispatcher("/Suggest.jsp").forward(request,response);
    }
    private boolean isValid(String name, String category, String description){
       if(name.equals("")||category.equals("")||description.equals("")){
           return false;
       } else {
           return true;
       }
    }
}
