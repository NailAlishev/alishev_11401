/**
 * Created by neil on 20.10.15.
 */
public class Place {
    private String name;
    private String description;
    private int category;

    public Place(String name, String description, int category) {
        this.name = name;
        this.description = description;
        this.category = category;
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getCategory() {
        return category;
    }
}