import java.sql.*;
import java.util.ArrayList;

/**
 * Created by neil on 20.10.15.
 */
public class PlacesRepository {

    public static ArrayList<Place> getPlacesbyCategory(String category) {
        ArrayList<Place> places = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semplacesdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs;
            if (category.equals("all")) {
                rs = stmt.executeQuery("SELECT * FROM PLACES");
                while (rs.next()) {
                    places.add(new Place(rs.getString("place_name"), rs.getString("place_description"), rs.getInt("categoryid")));
                }
                return places;
            } else {
                rs = stmt.executeQuery("SELECT categoryid from category WHERE category_name = '" + category + "'");
                rs.next();
                int categoryid = rs.getInt("categoryid");
                rs = stmt.executeQuery("SELECT * FROM PLACES WHERE categoryid = " + categoryid);
                while (rs.next()) {
                    places.add(new Place(rs.getString("place_name"), rs.getString("place_description"), rs.getInt("categoryid")));
                }
                return places;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void newPlace(String name, String description, String category) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semplacesdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT categoryid from category where category_name = " + "'" + category + "'");
            rs.next();
            int categoryid = rs.getInt("categoryid");
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO PLACES(place_name,place_description,categoryid) " +
                    "VALUES(?,?,?)");
            preparedStatement.setString(1,name);
            preparedStatement.setString(2,description);
            preparedStatement.setInt(3,categoryid);
            preparedStatement.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
