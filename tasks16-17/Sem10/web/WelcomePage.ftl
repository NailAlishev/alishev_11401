<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interest rating</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="styles/my.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<nav class="navbar navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">
    <div class="container-fluid" style="background-color:#CD5C5C;">
        <div class="col-md-3">
            <div class="navbar-header">
                <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default"
                            style="background-color:#CD5C5C; color:white; border-color: white;">Top trending
                    </button>
                    <button type="button" class="btn btn-default"
                            style="background-color:#CD5C5C; color:white; border-color: white;">Поиск
                    </button>
                    <button type="button" class="btn btn-default"
                            style="background-color:#CD5C5C; color:white; border-color: white;">Предложить заведение
                    </button>
                    <button type="button" class="btn btn-default"
                            style="background-color:#CD5C5C; color:white; border-color: white;">О нас
                    </button>
                </div>
                <button type="button" class="btn btn-primary navbar-btn"
                        style="margin-right: 10px; margin-left:325px; background-color:#CD5C5C; color:white; border-color: white;">
                    Войти
                </button>
                <button type="button" class="btn btn-primary navbar-btn"
                        style="background-color:#CD5C5C; color:white; border-color: white;">Регистрация
                </button>
            </div>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="col-md-3">
        <div class="filters" style="text-align:center;">
            <style>
                a:link {
                    color: rgba(22, 20, 112, 0.41);
                    text-decoration: none;
                }

                a:hover {
                    color: navy;
                    text-decoration: none;
                }

            </style>
            <a href="/welcome?category=restaurants"><h2 style="margin-bottom: 50px; margin-top: 50px;">Рестораны</h2></a>
            <hr/>
            <a href="/welcome?category=cafes"><h2 style="margin-bottom: 50px;">Кафе</h2></a>
            <hr/>
            <a href="/welcome?category=other"><h2 style="margin-bottom: 50px;">Другое</h2></a>
            <hr/>
        </div>
    </div>
    <div class="content col-md-9"><p><#list places as place><h1
            style=" color:rgba(94, 45, 48, 0.82);">${place.name}</h1> ${place.description} </p></#list></h1>
        <p style="text-align: center;"><a href="#">Наверх</a></p>
    </div>
</div>
<div class="footer navbar-bottom">
    <h1>It's a footer</h1>
</div>
</body>
</html>