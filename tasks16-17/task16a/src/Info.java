/**
 * Created by neil on 18.10.15.
 */
public class Info {
    private String student_name;
    private String class_name;
    private String year;

    public Info(String student_name, String class_name, String year) {
        this.student_name = student_name;
        this.class_name = class_name;
        this.year = year;
    }

    public Info(String student_name, String year) {
        this.student_name = student_name;
        this.year = year;
    }

    public String getStudent_name() {
        return student_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public String getYear() {
        return year;
    }
}