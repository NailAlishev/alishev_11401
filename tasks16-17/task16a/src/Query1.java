import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by neil on 17.10.15.
 */
public class Query1 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        Template tmpl = cfg.getTemplate("query1.ftl");
        HashMap<String, Object> root = new HashMap<>();
        ArrayList<Info> rows = new ArrayList<>();
        ArrayList<String> years = AttendanceRepository.getYears(1992,1998);
        ArrayList<String> students = AttendanceRepository.getStudentsByYear(1992,1998);
        ArrayList<String> classes = AttendanceRepository.getClassesByYear(1992,1998);
        for(int i = 0; i<years.size(); i++){
            rows.add(new Info(years.get(i),students.get(i),classes.get(i)));
        }

        root.put("rows",rows);
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}