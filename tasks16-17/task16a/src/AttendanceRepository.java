import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by neil on 17.10.15.
 */
public class AttendanceRepository {

    public static ArrayList<String> getStudentsByYear(int from, int to) {           //inclusive
        ArrayList<String> names = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                    ""
            );
            PreparedStatement stmt = conn.prepareStatement("SELECT students.name AS student_name " +
                    "FROM students INNER JOIN attendance ON students.id = attendance.student_id" +
                    " WHERE attendance.year>=? AND attendance.year<=? ");
            stmt.setInt(1, from);
            stmt.setInt(2, to);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                names.add(rs.getString("student_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return names;
    }


    public static ArrayList<String> getClassesByYear(int from, int to) {           //inclusive
        ArrayList<String> classes = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                    ""
            );
            PreparedStatement stmt = conn.prepareStatement("SELECT classes.name AS class_name " +
                    "FROM classes INNER JOIN attendance ON classes.id = attendance.class_id" +
                    " WHERE attendance.year>=? AND attendance.year<=? ");
            stmt.setInt(1, from);
            stmt.setInt(2, to);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                classes.add(rs.getString("class_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return classes;
    }


    public static ArrayList<String> getYears(int from, int to) {                    //inclusive
        ArrayList<String> years = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                    ""
            );
            PreparedStatement stmt = conn.prepareStatement("SELECT \"year\" " +
                    "FROM attendance " +
                    " WHERE attendance.year>=? AND attendance.year<=? ");
            stmt.setInt(1, from);
            stmt.setInt(2, to);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                years.add(rs.getString("year"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return years;
    }


    public static ArrayList<String> getStudentsByTeacher(int teacher_id) {
        ArrayList<String> students = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                    ""
            );
            PreparedStatement stmt = conn.prepareStatement("SELECT students.name AS student_name " +
                    "FROM students INNER JOIN attendance " +
                    "ON students.id = attendance.student_id " +
                    "WHERE attendance.teacher_id = ?");
            stmt.setInt(1, teacher_id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                students.add(rs.getString("student_name"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return students;
    }

    public static ArrayList<String> getYearsByTeacher(int teacher_id){
        ArrayList<String> years = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                    ""
            );
            PreparedStatement stmt = conn.prepareStatement("SELECT \"year\" " +
                    "FROM students INNER JOIN attendance " +
                    "ON students.id = attendance.student_id " +
                    "WHERE attendance.teacher_id = ?");
            stmt.setInt(1, teacher_id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                years.add(rs.getString("year"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return years;
    }

}
