import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by neil on 17.10.15.
 */
@WebServlet(name = "Query2")
public class Query2 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        Template tmpl = cfg.getTemplate("query2.ftl");
        HashMap<String, Object> root = new HashMap<>();
        ArrayList<Info> rows = new ArrayList<>();
        ArrayList<String> students = AttendanceRepository.getStudentsByTeacher(2);
        ArrayList<String> years = AttendanceRepository.getYearsByTeacher(2);
        for(int i = 0; i<years.size(); i++){
            rows.add(new Info(students.get(i),years.get(i)));
        }
        root.put("rows",rows);
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}