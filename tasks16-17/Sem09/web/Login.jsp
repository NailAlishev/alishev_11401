<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 19.10.15
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Войти</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="styles/css/signin.css" rel="stylesheet" type="text/css">
</head>
<body style="background-color: white;">
<% if (request.getParameter("errorMessage") != null) {%>
<strong><%=request.getParameter("errorMessage")%>
</strong> </br>
<%if (request.getParameter("username").matches("\\w+@\\w+\\.\\w+")) {%>
username was: <%=request.getParameter("username")%>
<%
        }
    }
%>

<div class="container">
    <form class="form-signin" action="/login" method="POST">
        <h2 class="form-signin-heading" style="color: #CD5C5C">Войти</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" required>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="rememberMe" value="yes"> Запомнить меня
            </label>
        </div>
        <button class="btn btn-lg btn-default btn-block" type="submit" style="background-color:#CD5C5C; color: white">
            Войти
        </button>
    </form>
</div>
</body>
</html>