<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 19.10.15
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Регистрация</title>
  <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="styles/css/signin.css" rel="stylesheet" type="text/css">
</head>
<body style="background-color: white;">
<% if (request.getParameter("errorMessage") != null) {%>
<strong><%=request.getParameter("errorMessage")%>
</strong><%}%> </br>
<div class="container">
  <form action="/registration" method="POST" class="form-signin">
    <h2 class="form-signin-heading" style="color: #CD5C5C">Регистрация</h2>
    <!--<input type="text" class="form-control" placeholder="Имя" required autofocus>-->
    <input type="text" name="email" id="inputEmail" class="form-control" placeholder="Email">
    <input type="password" name="password" class="form-control" placeholder="Пароль" required style="margin-bottom: 0px;">
    <!--<input type="password" class="form-control" placeholder="Подтвердите пароль" required>-->
    <button class="btn btn-lg btn-default btn-block" type="submit" style="background-color:#CD5C5C; color: white">Зарегистрироваться</button>
  </form>
</div>
</body>
</html>