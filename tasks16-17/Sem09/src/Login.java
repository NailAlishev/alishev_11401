import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by neil on 19.10.15.
 */
@WebServlet(name = "Login")
public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession hs = request.getSession();
        if (hs.getAttribute("current_user") != null) {
            response.sendRedirect("/profile");
        } else {                                                   // все проверки засунуть в isValid() метод
            if ((!request.getParameter("email").equals("")) && (!request.getParameter("password").equals(""))) {
                if (UsersRepository.isRegistered(request.getParameter("email"),request.getParameter("password"))) {
                    hs.setAttribute("current_user", request.getParameter("email"));
                    if (request.getParameter("rememberMe") != null) {
                        Cookie cookie = new Cookie("current_user", request.getParameter("email"));
                        cookie.setMaxAge(24 * 60 * 60);
                        response.addCookie(cookie);
                    }
                    response.sendRedirect("/profile");

                } else {
                    response.sendRedirect("/login?errorMessage=User doesn't exist&username=" + request.getParameter("email"));
                }
            } else {
                response.sendRedirect("/login?errorMessage=Invalid input&username=" + request.getParameter("email"));
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {

            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals("current_user")) {
                        session.setAttribute("current_user", cookie.getValue());
                        response.sendRedirect("/login");
                    }
                }
            }


            getServletContext().getRequestDispatcher("/Login.jsp").forward(request,response);
        }
    }
}
