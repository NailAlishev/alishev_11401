import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by neil on 19.10.15.
 */
public class UsersRepository {
    public static void newUser(String email, String password) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/semuserdatabase", "sem_user",
                            ""
                    );
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO USERS(email,password) VALUES (?,?)");
            stmt.setString(1, email);
            stmt.setString(2, password);
            stmt.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean isRegistered(String email, String password) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/semuserdatabase", "sem_user",
                            ""
                    );
            PreparedStatement stmt = conn.prepareStatement("SELECT password FROM USERS WHERE (email = ?)");
            stmt.setString(1,email);
            ResultSet rs = stmt.executeQuery();
            boolean emailExists = rs.next();
            if (emailExists && password.equals(rs.getString("password"))) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
