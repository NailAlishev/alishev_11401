package tables;

/**
 * Created by neil on 18.10.15.
 */
public class Attendance {
    Classes classes;
    int class_id;
    Teachers teacher;
    int teacher_id;
    int year;
    Students student;
    int student_id;

    public Attendance(Classes classes, int class_id, Teachers teacher, int teacher_id, int year, Students student, int student_id) {
        this.classes = classes;
        this.class_id = class_id;
        this.teacher = teacher;
        this.teacher_id = teacher_id;
        this.year = year;
        this.student = student;
        this.student_id = student_id;
    }

    public Classes getClasses() {
        return classes;
    }

    public int getClass_id() {
        return class_id;
    }

    public Teachers getTeacher() {
        return teacher;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public int getYear() {
        return year;
    }

    public Students getStudent() {
        return student;
    }

    public int getStudent_id() {
        return student_id;
    }
}
