package tables;

/**
 * Created by neil on 18.10.15.
 */
public class Schools {
    int id;
    String name;

    public Schools(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
