package tables;

/**
 * Created by neil on 18.10.15.
 */
public class Teachers {
    int id;
    String name;
    Schools school;
    int school_id;

    public Teachers(int id, String name, int school_id, Schools school) {
        this.id = id;
        this.name = name;
        this.school_id = school_id;
        this.school = school;
    }

    public String getName() {
        return name;
    }

    public Schools getSchool() {
        return school;
    }

    public int getId() {
        return id;

    }
}
