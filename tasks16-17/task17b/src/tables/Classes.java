package tables;

/**
 * Created by neil on 18.10.15.
 */
public class Classes {
    int id;
    String name;

    public Classes(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }
}
