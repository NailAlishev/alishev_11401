import jdk.nashorn.internal.runtime.ECMAException;
import tables.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;

/**
 * Created by neil on 18.10.15.
 */
public class Test {
    static HashSet<Students> students = new HashSet<Students>();
    static HashSet<Classes> classes = new HashSet<Classes>();
    static HashSet<Schools> schools = new HashSet<Schools>();
    static HashSet<Teachers> teachers = new HashSet<Teachers>();
    static HashSet<Attendance> attendance = new HashSet<Attendance>();


    public static void main(String[] args) throws Exception {
        insert();
        //--------------------------------------------Первый запрос--------------------------------
        for (Attendance a : attendance) {
            if (a.getYear() >= 1992 && a.getYear() <= 1998) {
                for (Students s : students) {
                    if (s.getId() == a.getStudent_id()) {
                        System.out.print(s.getName()+" - ");
                        break;
                    }
                }
                for(Classes c:classes){
                    if(c.getId()==a.getClass_id()){
                        System.out.print(c.getName() + " - ");
                        break;
                    }
                }
                System.out.println(a.getYear());

            }
        }
        //----------------------------------------Второй запрос--------------------------------------
        System.out.println();
        for(Attendance a :attendance){
            if(a.getTeacher_id()==2){
                for(Students s:students){
                    if(s.getId()==a.getStudent_id()){
                        System.out.print(s.getName()+" - ");
                        break;
                    }
                }
                System.out.println(a.getYear());
            }
        }

    }

    public static void insert() throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                ""
        );
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM STUDENTS");
        while (rs.next()) {
            students.add(new Students(rs.getInt("ID"), rs.getString("NAME")));
        }
        rs = stmt.executeQuery("SELECT * FROM CLASSES");
        while (rs.next()) {
            classes.add(new Classes(rs.getInt("ID"), rs.getString("NAME")));
        }
        rs = stmt.executeQuery("SELECT * FROM SCHOOLS");
        while (rs.next()) {
            schools.add(new Schools(rs.getInt("ID"), rs.getString("NAME")));
        }
        rs = stmt.executeQuery("SELECT * FROM TEACHERS");
        while (rs.next()) {
            Schools school = null;
            for (Schools s : schools) {
                if (s.getId() == rs.getInt("school_id")) {
                    school = s;
                    break;
                }
            }
            teachers.add(new Teachers(rs.getInt("ID"), rs.getString("NAME"), rs.getInt("school_id"), school));
        }
        rs = stmt.executeQuery("SELECT * FROM ATTENDANCE");
        while (rs.next()) {
            Classes c = null;
            for (Classes c1 : classes) {
                if (c1.getId() == rs.getInt("class_id")) {
                    c = c1;
                    break;
                }
            }
            Teachers teacher = null;
            for (Teachers t : teachers) {
                if (t.getId() == rs.getInt("teacher_id")) {
                    teacher = t;
                    break;
                }
            }

            Students student = null;
            for (Students s : students) {
                if (s.getId() == rs.getInt("student_id")) {
                    student = s;
                    break;
                }
            }


            attendance.add(new Attendance(c, rs.getInt("class_id"), teacher, rs.getInt("teacher_id"), rs.getInt("year"), student, rs.getInt("student_id")));
        }
    }
}
