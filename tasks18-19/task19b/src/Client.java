
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author Nail Alishev
 *         11-401
 *         task 19b
 */
public class Client {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        final int PORT = 3456;
        final String HOST = "localhost";
        Socket socket = new Socket(HOST, PORT);
        PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        while (true) {
            System.out.println("Server: " + br.readLine());
            pw.println(scanner.nextLine());
        }
    }
}
