import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Scanner;


/**
 * @author Nail Alishev
 *         11-401
 *         task 19b
 */
public class Server {
    private static HashSet<String> cities = new HashSet<String>();
    private static boolean flag = true;

    public static void main(String[] args) throws IOException {
        final int PORT = 3456;
        ServerSocket socket = new ServerSocket(PORT);
        Socket client = socket.accept();
        PrintWriter pw = new PrintWriter(client.getOutputStream(), true);
        BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
        Scanner scanner = new Scanner(System.in);
        String serverCity;
        String clientCity = null;
        while (true) {
            serverCity = scanner.nextLine();
            while (!isValid(serverCity, clientCity)) {
                System.out.println("Try again!");
                serverCity = scanner.nextLine();
            }
            cities.add(serverCity);
            pw.println(serverCity);
            clientCity = br.readLine();
            while (!isValid(clientCity, serverCity)) {
                pw.println("Try again!");
                clientCity = br.readLine();
            }
            System.out.println("Client: " + clientCity);
            cities.add(clientCity);
        }

    }

    private static boolean isValid(String city1, String city2) {
        if (flag) {
            flag = false;
            return true;
        }
        return (!cities.contains(city1)) && (Character.toLowerCase(city1.charAt(0)) == Character.toLowerCase(city2.charAt(city2.length() - 1)));
    }
}
