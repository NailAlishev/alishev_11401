import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by neil on 02.11.15.
 */
@WebServlet(name = "Servlet")
public class Servlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/json");
        PrintWriter out = response.getWriter();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/virtual_schools", "test_user",
                            ""
                    );
            String q = request.getParameter("q");
            if (!q.equals("")) {
                String select = request.getParameter("select");
                PreparedStatement ps = conn.prepareStatement("select name from "+select+" where name like ?");
                ps.setString(1, q + "%");
                ResultSet rs = ps.executeQuery();
                JSONArray ja = new JSONArray();
                while (rs.next()) {
                    ja.put(rs.getString("name"));
                }
                JSONObject jo = new JSONObject();
                jo.put("results", ja);
                out.print(jo.toString());
            } else{
                response.getWriter().print(new JSONObject().put("results", new JSONArray()).toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
