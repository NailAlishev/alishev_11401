<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 02.11.15
  Time: 22:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="application/javascript" src="Jquery/jquery-2.1.4.min.js"></script>
    <script type="application/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<input type="text" id="s" oninput="f()"/>
<span class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Choose table
        <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a onclick="change('students')">Students</a></li>
        <li><a onclick="change('teachers')">Teachers</a></li>
        <li><a onclick="change('classes')">Classes</a></li>
    </ul>
    <input id="select" type="hidden" value="students" />
</span>

<div id="res"></div>

<script type="application/javascript">
    f = function (request, response) {
        $.ajax({
            url: "/servlet",
            data: {"q": $("#s").val(), "select": $("#select").val()},
            dataType: "json",
            success: function (data) {
                if (data.results) {
                    $("#res").html("Search results:");
                    for (var i = 0; i < data.results.length; i++) {
                        $("#res").append("<li>" + data.results[i] + "</li>");
                    }
                } else {
                    $("#res").html("No results");
                }
            }
        });
    }

    change = function (param) {
        $("#select").val(param);
    }
</script>
</body>
</html>
