import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author Nail Alishev
 *         11-401
 *         task 19c
 */
public class Server {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name");
        String name = scanner.nextLine();
        final int PORT = 3456;
        ServerSocket s = new ServerSocket(PORT);
        System.out.println("Starting listening on port " + PORT);
        Socket client = s.accept();
        System.out.println("Client connected");
        OutputStream os = client.getOutputStream();
        PrintWriter pw = new PrintWriter(os, true);
        InputStream is = client.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        pw.println(name);
        String clientName = br.readLine();
        while (true) {
            String x = scanner.nextLine();
            pw.println(x);
            String y = br.readLine();
            System.out.println(clientName + ": " + y);
        }
    }
}

