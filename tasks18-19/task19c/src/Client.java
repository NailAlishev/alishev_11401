import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author Nail Alishev
 *         11-401
 *         task 19c
 */
public class Client {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name!");
        String name = scanner.nextLine();
        final int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);
        OutputStream os = s.getOutputStream();
        PrintWriter pw = new PrintWriter(os, true);
        InputStream is = s.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        pw.println(name);
        String serverName = br.readLine();
        while (true) {
            String x = br.readLine();
            System.out.println(serverName + ": " + x);
            String y = scanner.nextLine();
            pw.println(y);
        }

    }
}
