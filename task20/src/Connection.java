import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Nail Alishev
 *         11-401
 *         task 20b
 */
public class Connection implements Runnable {
    private Socket socket;
    private Thread thread;
    private Server server;
    private static ArrayList<String> messages = new ArrayList<String>();

    public Connection(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;

        thread = new Thread(this);
        thread.start();
    }

    public void run() {
        PrintWriter pw = null;
        BufferedReader br = null;
        try {
            System.out.println("Client connected");
            OutputStream os = socket.getOutputStream();
            pw = new PrintWriter(os, true);

            InputStream is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String y;
        String messagesRead;
        while (true) {
            try {
                y = br.readLine();

                if (messages.size() == 0) {
                    pw.println(0);
                    br.readLine();
                } else {
                    pw.println(messages.size());
                    messagesRead = br.readLine();
                    for (int i = Integer.parseInt(messagesRead); i < messages.size(); i++) {
                        pw.println(messages.get(i));
                    }
                }
                addMessage(y);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void addMessage(String msg) {
        messages.add(msg);
    }
}
