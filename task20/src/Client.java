import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author Nail Alishev
 *         11-401
 *         task 20b
 */
public class Client {

    public static void main(String[] args) throws IOException {
        final int port = 3456;
        final String host = "localhost";

        Socket s = new Socket(host, port);

        OutputStream os = s.getOutputStream();
        PrintWriter pw = new PrintWriter(os, true);

        InputStream is = s.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        Scanner scanner = new Scanner(System.in);
        String x;
        int messagesRead = 0;
        int messagesReadTemp;
        while (true) {
            String y = scanner.nextLine();
            pw.println(y);
            x = br.readLine();
            pw.println(messagesRead);
            messagesReadTemp = messagesRead;
            for (int i = 0; i < Integer.parseInt(x) - messagesRead; i++) {
                System.out.println(br.readLine());
                messagesReadTemp++;
            }
            messagesRead = messagesReadTemp;


        }
    }
}