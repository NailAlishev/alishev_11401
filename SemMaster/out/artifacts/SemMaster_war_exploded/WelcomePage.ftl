<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interest rating</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="styles/my.css" rel="stylesheet" type="text/css"/>
    <script type="application/javascript" src="Jquery/jquery-2.1.4.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
        <#if current_user??>
            <li><a href="/profile" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Мой профиль
            </a></li>
            <li><a href="/logout" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Выйти
            </a></li>
        <#else>
            <li><a href="/login" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Войти
            </a></li>
            <li>
                <a href="/registration" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                </a></li>
        </#if>
        </ul>
    </div>

</nav>
<div class="container-fluid">
    <div class="col-md-3">
        <div class="filters" style="text-align:center;">
            <style>
                a:link {

                    text-decoration: none;
                }

                a:hover {
                    color: #CD5C5C;
                    text-decoration: none;
                }

            </style>
            <a href="/welcome?category=restaurants"><h2
                    style="margin-bottom: 50px; margin-top: 50px;">Рестораны</h2>
            </a>
            <hr/>
            <a href="/welcome?category=cafes"><h2 style="margin-bottom: 50px;">Кафе</h2></a>
            <hr/>
            <a href="/welcome?category=cinemas"><h2 style="margin-bottom: 50px;">Кинотеатры</h2></a>
            <hr/>
            <a href="/welcome?category=art"><h2 style="margin-bottom: 50px;">Искусство</h2></a>
            <hr/>
            <a href="/welcome?category=other"><h2 style="margin-bottom: 50px;">Другое</h2></a>
            <hr/>
        </div>
    </div>
    <div class="content col-md-9"><p><#list places as place><h1
            style=" color:rgba(94, 45, 48, 0.82);"><a href="/place?id=${place.id}">${place.name}</a></h1>

        <p> ${place.description}<br/></p></#list>
    </div>
</div>
<div class="footer navbar-bottom">
    <h1>It's a footer</h1>
</div>


</body>
</html>