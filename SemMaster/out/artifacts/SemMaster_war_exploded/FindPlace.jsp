<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 06.11.15
  Time: 13:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Найти</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css">
    <script type="application/javascript" src="Jquery/jquery-2.1.4.min.js"></script>
    <script type="application/javascript" src="styles/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <%if (session.getAttribute("current_user") != null) {%>
            <li><a href="/profile" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Мой профиль
            </a></li>
            <li><a href="/logout" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Выйти
            </a></li>
            <%} else {%>
            <li><a href="/login" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Войти
            </a></li>
            <li>
                <a href="/registration" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                </a></li>
            <%}%>
        </ul>
    </div>

</nav>
<h1 style="color: #CD5C5C; width:300px; margin-top: 50px; margin-left: auto; margin-right: auto;">Найти в Казани:</h1>

<div class="input-group">
    <input type="text" id="s" oninput="f()" class="form-control" style="width:700px; position: absolute; left: 250px;">
        <span class="dropdown" style="position: absolute; left:960px;">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
            style="background-color: white; border-color: #CD5C5C; color:#CD5C5C;">Выберите категорию
        <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a onclick="change('cafes')">Кафе</a></li>
        <li><a onclick="change('restaurants')">Рестораны</a></li>
        <li><a onclick="change('cinemas')">Кинотеатры</a></li>
        <li><a onclick="change('art')">Искусство</a></li>
        <li><a onclick="change('other')">Другое</a></li>
    </ul>
    <input id="select" type="hidden" value="default"/>
</span>
    <br/>
    <br/>

    <div id="res"></div>

    <script type="application/javascript">
        f = function (request, response) {
            $.ajax({
                        url: "/findServlet",
                        type: 'GET',
                        data: {"q": $("#s").val(), "s": $("#select").val()},
                        dataType: "json",
                        success: function (response_data) {
                            if (response_data.results.length !== 0) {
                                $("#res").html("<h1>Результаты поиска</h1>");
                                for (var i = 0; i < response_data.results.length; i++) {
                                    $("#res").append("<li>" + response_data.results[i] + "</li>")
                                }
                            } else {
                                $("#res").html("");
                            }
                        }
                    }
            );
        };

        change = function (param) {
            $("#select").val(param);
            f();
        }

    </script>
    <!--<span class="input-group-btn">
      <button class="btn btn-default" type="button" style="background-color:#CD5C5C; color:white; ">Найти</button>
    </span>-->
</div>

</body>
</html>
