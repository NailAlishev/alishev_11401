<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 06.11.15
  Time: 13:38
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Предложить объект</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet">
    <link href="styles/my.css" rel="stylesheet">
    <script type="application/javascript" src="Jquery/jquery-2.1.4.min.js"></script>
    <script type="application/javascript" src="Jquery/jquery.validate.js"></script>
    <script type="application/javascript" src="Jquery/suggest_form.js"></script>
    <script type="application/javascript" src="Jquery/textarea.js"></script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <%if(session.getAttribute("current_user")!=null){%>
                <li><a href="/profile" class="btn btn-primary navbar-btn"
                       style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                    Мой профиль
                </a></li>
                <li><a href="/logout" class="btn btn-primary navbar-btn"
                       style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                    Выйти
                </a></li>
                <%}else{%>
                    <li><a href="/login" class="btn btn-primary navbar-btn"
                           style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                        Войти
                    </a></li>
                    <li>
                        <a href="/registration" class="btn btn-primary navbar-btn"
                           style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                        </a></li>
            <%}%>
        </ul>
    </div>

</nav>
<h1 style="color: #CD5C5C; position: fixed; left: 500px; top:100px;">Предложите объект</h1>

<div class="input-group">
    <form action="/suggest" method="POST" id="suggestForm">
        <input type="text" name="placeName" class="form-control" placeholder="Название" style="width:360px;
      position: fixed; left: 500px; top:200px;">
        <select name="category" class="form-control" style="width: 360px;position: fixed; left: 500px; top:250px;">
            <option value="cafes">Кафе</option>
            <option value="restaurants">Ресторан</option>
            <option value="cinemas">Кинотеатр</option>
            <option value="art">Искусство</option>
            <option value="other">Другое</option>
        </select>
    <textarea name="description" id="description" rows="4" maxlength="1000" style="width:360px;
      position: fixed; left: 500px; top:300px;"></textarea>

        <div id="textCount" style="position:fixed; left:700px; top: 390px;"></div>
        <input type="text" id="address" name="address" class="form-control" placeholder="Адрес" style="width:360px;
          position: fixed; left: 500px; top:420px;">
        <input type="text" id="phone" name="phone" class="form-control" placeholder="Телефон" style="width:360px;
          position: fixed; left: 500px; top:470px;">
        <input type="text" id="site" name="site" class="form-control" placeholder="Сайт" style="width:360px;
          position: fixed; left: 500px; top:520px;">


        <input type="submit" class="btn btn-default" value="Отправить" style="width:360px;
      position: fixed; left: 500px; top:570px; background-color:#CD5C5C; color: white; ">
    </form>
</div>
</body>
</html>