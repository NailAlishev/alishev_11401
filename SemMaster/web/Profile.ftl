<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Профиль</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
        <#if current_user??>
            <li><a href="/profile" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Мой профиль
            </a></li>
            <li><a href="/logout" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Выйти
            </a></li>
        <#else>
            <li><a href="/login" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Войти
            </a></li>
            <li>
                <a href="/registration" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                </a></li>
        </#if>
        </ul>
    </div>

</nav>
<div class="container-fluid" style="margin-top: 150px;">
    <img src="photos/gray.png" class="img-circle" width="200px" height="200px" caption="User"/>
    <span style="font-size: 350%; font-family: helvetica, sans-serif; margin-left: 50px;">${name}</span>
    </br>
    <nav class="navbar navbar-default col-md-4"
         style="margin-top: 30px; background-color:white; margin-left: 20px; border-color: #CD5C5C; padding: 0px;">
        <div>
            <ul class="nav navbar-nav">
                <li><a href="#" style="color: #CD5C5C;">Профиль</a></li>
                <li class="active"><a href="#" style="color: #CD5C5C;">Закладки</a></li>
                <li><a href="#" style="color: #CD5C5C;">Отзывы и оценки</a></li>
                <li><a href="#" style="color: #CD5C5C;">Настройки</a></li>
            </ul>
        </div>
    </nav>
</div>
<#if bookmarks??>
    <#list bookmarks as bookmark>
    <div style="margin-left: auto; margin-right: auto; width:500px; text-align: center; " >
    <h1 style=" color:rgba(94, 45, 48, 0.82);"><a
            href="/place?id=${bookmark.id}">${bookmark.name}</a></h1></div>
            <hr />
    </#list>
<#else>
<h4 style="margin-top: 100px; margin-left:auto; margin-right:auto; color:gray">У вас нет объектов в закладках</h4>
</#if>
</body>
</html>