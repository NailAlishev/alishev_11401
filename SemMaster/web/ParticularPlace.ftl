<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Заведение</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet">
    <script type="application/javascript" src="Jquery/jquery-2.1.4.min.js"></script>
    <script type="application/javascript" src="styles/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
        <#if current_user??>
            <li><a href="/profile" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Мой профиль
            </a></li>
            <li><a href="/logout" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Выйти
            </a></li>
        <#else>
            <li><a href="/login" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Войти
            </a></li>
            <li>
                <a href="/registration" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                </a></li>
        </#if>
        </ul>
    </div>

</nav>
<div style="border: thin solid; width:1000px; text-align: center; border-color: #CD5C5C; background: white; margin-top: 150px; margin-left: auto; margin-right: auto; ">
    <h1 style="font-family: helvetica, sans-serif; font-size: 500%; color: #CD5C5C">${place.name}
        <h1>
            <p style="margin-right: 30px; font-size: 50%; color:black;">Позиция в рейтинге: ${rating}</p>
        <#if current_user??>
            <button style="font-size: 15px; position:relative; right:180px;" class="bookmark btn btn-default btn-xs"
                    onclick="addBookmark()">Добавить в закладки
            </button>
               <span style="position:relative; left:170px;">
                <span style="font-size: 30px; color:gray; ">Ваша оценка :</span>
            <span class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
            style="background-color: white; border-color: #CD5C5C; color:#CD5C5C;">Выберите оценку
        <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a onclick="change('5')">Отлично</a></li>
        <li><a onclick="change('4')">Хорошо</a></li>
        <li><a onclick="change('3')">Удовлетворительно</a></li>
        <li><a onclick="change('2')">Плохо</a></li>
    </ul>
                <span id="res" style="font-size: 10px; visibility: hidden;">Сохранено</span>
    <input id="select" type="hidden" value="default"/>
</span></span>
        </#if>
</div>

<div style="margin-left: 20px; margin-top:50px;">
    <p>${place.description}</p>
</div>
<div class="contacts" style="margin-left: 20px; margin-top: 50px;">
<#if place.address != "" >
    <h3>Адрес:</h3>

    <p>${place.address}</p>
    <br/>
</#if>
<#if place.phone != "" >
    <h3>Телефон:</h3>

    <p>${place.phone}</p>
    <br/>
</#if>
<#if place.site != "" >
    <h3>Сайт:</h3>

    <p>${place.site}</p>
    <br/>
</#if>
</div>
<!--<div class="comments" style="margin-left: 20px; margin-top: 50px;">
    <h1>Здесь будут комментарии</h1>
</div>-->
<script type="application/javascript">

    change = function (param) {
        $("#select").val(param);
        $.ajax({
                    url: "/place",
                    type: 'POST',
                    data: {"s": $("#select").val()},
                    dataType: "json"
                }
        );
        $("#res").css('visibility', 'visible').hide().fadeIn('slow');
    }


    addBookmark = function () {
        var userid = ${id};
        $.ajax({
                    url: "/addBookmark",
                    type: 'GET',
                    data: {"id": userid},
                    dataType: "json"
                }
        );

        $(".bookmark").removeClass('btn-default').addClass('btn-success');

    }


</script>
</body>
</html>