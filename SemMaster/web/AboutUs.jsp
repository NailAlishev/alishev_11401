<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 11.11.15
  Time: 5:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="styles/my.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<nav class="navbar navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <%if(session.getAttribute("current_user")!=null){%>
            <li><a href="/profile" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Мой профиль
            </a></li>
            <li><a href="/logout" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Выйти
            </a></li>
            <%}else{%>
            <li><a href="/login" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Войти
            </a></li>
            <li>
                <a href="/registration" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                </a></li>
            <%}%>
        </ul>
    </div>

</nav>
<h1>Первая семестровая работа в группе у М.М.Абрамского </h1>
</body>
</html>
