<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 05.11.15
  Time: 22:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Регистрация</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="styles/css/signin.css" rel="stylesheet" type="text/css">
    <script type="application/javascript" src="Jquery/jquery-2.1.4.min.js"></script>
    <script type="application/javascript" src="Jquery/jquery.validate.js"></script>
    <script type="application/javascript" src="Jquery/signup_form.js"></script>
</head>
<body style="background-color: white;">
<nav class="navbar navbar-fixed-top navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <%if(session.getAttribute("current_user")!=null){%>
            <li><a href="/profile" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Мой профиль
            </a></li>
            <li><a href="/logout" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Выйти
            </a></li>
            <%}else{%>
            <li><a href="/login" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Войти
            </a></li>
            <li>
                <a href="/registration" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                </a></li>
            <%}%>
        </ul>
    </div>

</nav>
<% if (request.getParameter("errorMessage") != null) {%>
<strong><%=request.getParameter("errorMessage")%>
</strong><%}%> </br>
<div class="container">
    <form action="/registration" method="POST" class="form-signin" id="signupForm">
        <h2 class="form-signin-heading" style="color: #CD5C5C">Регистрация</h2>
        <input type="text" id="loginName" name="loginName" class="form-control" placeholder="Имя" autofocus>
        <input type="email" id="email" name="email" class="form-control" placeholder="Email">
        <input type="password" id="password" name="password" class="form-control" placeholder="Пароль"
               style="margin-bottom: 0px;">
        <input type="password" id="confirm_password" name="confirm_password" class="form-control"
               placeholder="Подтвердите пароль">
        <button class="btn btn-lg btn-default btn-block" type="submit" style="background-color:#CD5C5C; color: white">
            Зарегистрироваться
        </button>
    </form>
</div>
</body>
</html>



