<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 05.11.15
  Time: 22:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Войти</title>
    <link href="styles/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="styles/css/signin.css" rel="stylesheet" type="text/css">
</head>
<body style="background-color: white;">
<nav class="navbar navbar-fixed-top navbar-default" style="background-color:#CD5C5C; margin-bottom: 5px">

    <div class="col-md-3">
        <div class="navbar-header">
            <a class="navbar-brand" href="/welcome" style="font-family: Trebuchet MS;
              color:white;
            font-size: 30px">Interest Rating</a>
        </div>
    </div>
    <div class="col-md-9">

        <ul class="nav navbar-nav">
            <li><a href="/top" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Top
                trending
            </a></li>

            <li><a href="/find" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; padding:7px; border-color: white; margin-right:20px;">Поиск
            </a></li>

            <li><a href="/suggest" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">Предложить
                заведение
            </a></li>
            <li><a href="/about" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:20px;">О
                нас
            </a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <%if(session.getAttribute("current_user")!=null){%>
            <li><a href="/profile" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Мой профиль
            </a></li>
            <li><a href="/logout" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Выйти
            </a></li>
            <%}else{%>
            <li><a href="/login" class="btn btn-primary navbar-btn"
                   style="margin-right: 10px; background-color:#dc706e; color:white; border-color: white; padding:7px;">
                Войти
            </a></li>
            <li>
                <a href="/registration" class="btn btn-primary navbar-btn"
                   style="background-color:#dc706e; color:white; border-color: white; padding:7px; margin-right:10px;">Регистрация
                </a></li>
            <%}%>
        </ul>
    </div>

</nav>
<br/>
<br/>
<% if (request.getParameter("errorMessage") != null) {%>
<strong><%=request.getParameter("errorMessage")%>
</strong> </br>
<%if (request.getParameter("username").matches("\\w+@\\w+\\.\\w+")) {%>
username was: <%=request.getParameter("username")%>
<%
        }
    }
%>

<div class="container">
    <form class="form-signin" action="/login" method="POST">
        <h2 class="form-signin-heading" style="color: #CD5C5C">Войти</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" required>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="rememberMe" value="yes"> Запомнить меня
            </label>
        </div>
        <button class="btn btn-lg btn-default btn-block" type="submit" style="background-color:#CD5C5C; color: white">
            Войти
        </button>
    </form>
</div>
</body>
</html>