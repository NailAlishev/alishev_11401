$().ready(function () {
    $("#suggestForm").validate({
        errorElement: "div",
        errorClass: "my-error-class",

        rules: {
            placeName: {
                required: true,
                placeName: true
            },

            address: {
                address: true
            },
            phone: {
                phone: true
            },

            site: {
                site: true
            }
        },
        messages: {
            placeName: {
                required: "Введите название"
            }
        }
    })
});

jQuery.validator.addMethod("placeName", function (value, element) {
    return /^[a-zA-Zа-яА-Я0-9 -]{1,50}$/.test(value);
}, "Название заведения может содержать только буквы и иметь длину не более 50 символов");

jQuery.validator.addMethod("address", function (value, element) {
    return this.optional(element) || /^[уУ]л\.([а-яА-Я ]+),\d+$/.test(value);
}, "Введите адрес в формате ул.имя_улицы,номер_дома");

jQuery.validator.addMethod("phone", function (value, element) {
    return this.optional(element) || /^((\+7)|8)\d{10}$/.test(value);
}, "Введите номер телефона в формате 8xxxxxxxxxx");

jQuery.validator.addMethod("site", function (value, element) {
    return this.optional(element) || /^www\.\w+\.(com|ru)$/.test(value);
}, "Введите адрес сайта в формате www.адрес_сайта.(com или ru)");