$(document).ready(function () {
    var max_length = 1000;
    $("#textCount").text("Осталось " + max_length + " символов");

    $("#description").keyup(function () {
        var text_length = $("#description").val().length;
        $("#textCount").text("Осталось "+(max_length-text_length)+" символов");
    });
});