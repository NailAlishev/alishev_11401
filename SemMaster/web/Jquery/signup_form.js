$().ready(function () {
    $("#signupForm").validate({
        rules: {

            loginName: {
                required: true,
                loginName: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            loginName:{
                required: "Пожалуйста, введите имя"
            },

            email:{
                required: "Пожалуйста, введите email",
                email: "Пожалуйста, введите действительный email"
            },

            password: {
                required: "Пожалуйста, введите пароль",
                minlength: "Длина пароля должна быть больше 5 символов"
            },
            confirm_password: {
                required: "Пожалуйста, введите пароль",
                minlength: "Длина пароля должна быть больше 5 символов",
                equalTo: "Пароли должны совпадать"
            }
        }
    })
});

jQuery.validator.addMethod("loginName",function(value,element){
    return /^[a-zA-Zа-яА-Я]{1,20}$/.test(value);
}, "Введите правильное имя");