package ru.kpfu.itis.NAIL_ALISHEV.repository;

import ru.kpfu.itis.NAIL_ALISHEV.models.Place;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nail Alishev
 *         11-401
 */
public class PlacesRepository {

    public static ArrayList<Place> getPlacesbyCategory(String category) {
        ArrayList<Place> places = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs;
            if (category.equals("all")) {
                rs = stmt.executeQuery("SELECT * FROM PLACES");
                while (rs.next()) {
                    places.add(new Place(rs.getString("place_name"), rs.getString("place_description"), rs.getInt("categoryid"), rs.getInt("placeid")));
                }
                return places;
            } else {
                rs = stmt.executeQuery("SELECT categoryid from category WHERE category_name = '" + category + "'");
                rs.next();
                int categoryid = rs.getInt("categoryid");
                rs = stmt.executeQuery("SELECT * FROM PLACES WHERE categoryid = " + categoryid);
                while (rs.next()) {
                    places.add(new Place(rs.getString("place_name"), rs.getString("place_description"), rs.getInt("categoryid"), rs.getInt("placeid")));
                }
                return places;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void newPlace(String name, String description, String category, Map<String, String> additions) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT categoryid from category where category_name = " + "'" + category + "'");
            rs.next();
            int categoryid = rs.getInt("categoryid");
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO PLACES(place_name,place_description,categoryid,address,phone,website) " +
                    "VALUES(?,?,?,?,?,?)");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, description);
            preparedStatement.setInt(3, categoryid);
            preparedStatement.setString(4, additions.get("address"));
            preparedStatement.setString(5, additions.get("phone"));
            preparedStatement.setString(6, additions.get("site"));
            preparedStatement.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Place getPlaceByID(int id) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PLACES WHERE placeid = " + id);
            rs.next();
            HashMap<String,String> additions = new HashMap<>();
            additions.put("address",rs.getString("address"));
            additions.put("phone",rs.getString("phone"));
            additions.put("site",rs.getString("website"));
            return new Place(rs.getString("place_name"), rs.getString("place_description"), rs.getInt("categoryid"), additions);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
