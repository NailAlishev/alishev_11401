package ru.kpfu.itis.NAIL_ALISHEV.repository;

import ru.kpfu.itis.NAIL_ALISHEV.models.Place;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author Nail Alishev
 *         11-401
 */
public class BookmarksRepository {

    public static void addBookmark(String email, int placeid) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select userid from users where email = '" + email + "'");
            rs.next();
            int userid = rs.getInt("userid");
            stmt.executeUpdate("insert into bookmarks(placeid,userid) values(" + placeid + "," + userid + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static ArrayList<Place> getBookmarksByEmail(String email) {
        ArrayList<Place> bookmarks = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select userid from users where email = '" + email + "'");
            rs.next();
            int userid = rs.getInt("userid");
            rs = stmt.executeQuery("select placeid from bookmarks where userid =" + userid);
            while (rs.next()) {
                bookmarks.add(PlacesRepository.getPlaceByID(rs.getInt("placeid")).setId(rs.getInt("placeid")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookmarks.size() > 0 ? bookmarks : null;
    }

}
