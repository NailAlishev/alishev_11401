package ru.kpfu.itis.NAIL_ALISHEV.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.security.MessageDigest;

/**
 * @author Nail Alishev
 *         11-401
 */
public class UsersRepository {
    public static void newUser(String email, String password, String loginName) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/semdatabase", "sem_user",
                            ""
                    );
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(password.getBytes());
            String passHash = new String(md.digest());
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO USERS(email,password,name) VALUES (?,?,?)");
            stmt.setString(1, email);
            stmt.setString(2, passHash);
            stmt.setString(3, loginName);
            stmt.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean isRegistered(String email, String password) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/semdatabase", "sem_user",
                            ""
                    );
            PreparedStatement stmt = conn.prepareStatement("SELECT password FROM USERS WHERE (email = ?)");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();

            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(password.getBytes());
            String passHash = new String(md.digest());

            boolean emailExists = rs.next();
            if (emailExists && passHash.equals(rs.getString("password"))) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getNameByEmail(String email) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/semdatabase", "sem_user",
                            ""
                    );
            PreparedStatement stmt = conn.prepareStatement("SELECT name FROM USERS WHERE (email = ?)");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getString("name");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
