package ru.kpfu.itis.NAIL_ALISHEV.repository;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author Nail Alishev
 *         11-401
 */
public class FindRepository {

    public static JSONObject findPlaces(String q) {
        PreparedStatement ps;
        JSONObject jo = new JSONObject();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/semdatabase", "sem_user",
                            ""
                    );
            String params[] = q.split("&.=");
            if (!params[0].equals("") && params[1].equals("default")) {
                ps = conn.prepareStatement("select place_name from places where lower(place_name) like lower(?)");
            } else if ((!params[0].equals("")) && (!params[1].equals("default"))) {
                ps = conn.prepareStatement("select place_name from places where (lower(place_name) like lower(?)) AND categoryid = " +
                        "(select categoryid from category where category_name = '" + params[1] + "')");
            } else {
                return jo.put("results", new JSONArray());
            }
            ps.setString(1, params[0] + "%");
            ResultSet rs = ps.executeQuery();
            JSONArray ja = new JSONArray();
            while (rs.next()) {
                ja.put(rs.getString("place_name"));
            }
            jo.put("results", ja);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jo;
    }
}
