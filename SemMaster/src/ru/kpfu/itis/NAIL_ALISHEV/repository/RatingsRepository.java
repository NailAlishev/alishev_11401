package ru.kpfu.itis.NAIL_ALISHEV.repository;

import ru.kpfu.itis.NAIL_ALISHEV.models.Place;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by neil on 11.11.15.
 */
public class RatingsRepository {
    public static void newRating(int placeid, String userEmail, int rating) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select userid from users where email = '" + userEmail + "'");
            rs.next();
            int userid = rs.getInt("userid");
            stmt.executeQuery("insert into ratings(placeid, userid, score) values(" + placeid + "," + userid + "," + rating + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Place> PlacesByRating(String category) {
        ArrayList<Place> places = new ArrayList<Place>();
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select placeid, score from ratings");
            HashMap<Integer, Integer> ratings = new HashMap<>();
            while (rs.next()) {
                if (ratings.containsKey(rs.getInt("placeid"))) {
                    ratings.put(rs.getInt("placeid"), ratings.get(rs.getInt("placeid")) + rs.getInt("score"));
                } else {
                    ratings.put(rs.getInt("placeid"), rs.getInt("score"));
                }
            }
            for (int x : ratings.keySet()) {
                stmt.executeUpdate("update places set place_rating = " + ratings.get(x) + " where placeid = " + x);
            }


            if (category.equals("all")) {
                rs = stmt.executeQuery("SELECT * FROM PLACES ORDER BY place_rating DESC");
                while (rs.next()) {
                    places.add(new Place(rs.getString("place_name"), rs.getString("place_description"), rs.getInt("categoryid"), rs.getInt("placeid")));
                }
                return places;
            } else {
                rs = stmt.executeQuery("SELECT categoryid from category WHERE category_name = '" + category + "' ORDER BY place_rating DESC");
                rs.next();
                int categoryid = rs.getInt("categoryid");
                rs = stmt.executeQuery("SELECT * FROM PLACES WHERE categoryid = " + categoryid);
                while (rs.next()) {
                    places.add(new Place(rs.getString("place_name"), rs.getString("place_description"), rs.getInt("categoryid"), rs.getInt("placeid")));
                }
                return places;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int ratingById(int id) {
        int position = 1;
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/semdatabase", "sem_user", "");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select placeid, place_rating from places order by place_rating DESC");
            while (rs.next()) {
                if (rs.getInt("placeid") == id) {
                    break;
                } else {
                    position++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return position;
    }
}
