package ru.kpfu.itis.NAIL_ALISHEV.models;

import java.util.Map;


/**
 * @author Nail Alishev
 *         11-401
 */
public class Place {
    private String name;
    private String description;
    private int category;
    private String address = "";
    private String phone = "";
    private String site = "";
    private int id;


    public Place(String name, String description, int category, int id) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.id = id;
    }

    public Place(String name, String description, int category, Map<String, String> additions) {
        this.name = name;
        this.description = description;
        this.category = category;
        for (String addition : additions.keySet()) {
            switch (addition) {
                case "address":
                    this.address = additions.get(addition);
                    break;
                case "phone":
                    this.phone = additions.get(addition);
                    break;
                case "site":
                    this.site = additions.get(addition);
                    break;
            }
        }
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getCategory() {
        return category;
    }


    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getSite() {
        return site;
    }

    public int getId() {
        return id;
    }

    public Place setId(int id) {
        this.id = id;
        return this;
    }
}