package ru.kpfu.itis.NAIL_ALISHEV.servlets;

import ru.kpfu.itis.NAIL_ALISHEV.repository.BookmarksRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by neil on 16.11.15.
 */
@WebServlet(name = "AddBookmark")
public class AddBookmark extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int placeid = Integer.parseInt(request.getParameter("id"));
        HttpSession hs = request.getSession();
        String email = (String) hs.getAttribute("current_user");
        BookmarksRepository.addBookmark(email, placeid);
    }
}
