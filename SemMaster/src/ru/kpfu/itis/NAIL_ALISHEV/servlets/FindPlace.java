package ru.kpfu.itis.NAIL_ALISHEV.servlets;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.kpfu.itis.NAIL_ALISHEV.repository.FindRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * @author Nail Alishev
 *         11-401
 */
@WebServlet(name = "FindPlace")
public class FindPlace extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/json");
        response.getWriter().print(FindRepository.findPlaces(URLDecoder.decode(request.getQueryString(), "UTF-8").substring(2)).toString());

    }
}