package ru.kpfu.itis.NAIL_ALISHEV.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.NAIL_ALISHEV.repository.PlacesRepository;
import ru.kpfu.itis.NAIL_ALISHEV.repository.RatingsRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by neil on 10.11.15.
 */
@WebServlet(name = "ParticularPlace")
public class ParticularPlace extends HttpServlet {
    private int id;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession hs = request.getSession();
        RatingsRepository.newRating(id, (String) hs.getAttribute("current_user"), Integer.parseInt(request.getParameter("s")));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        HashMap<String, Object> root = new HashMap<>();
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmp = cfg.getTemplate("ParticularPlace.ftl");
        root.put("place", PlacesRepository.getPlaceByID(Integer.parseInt(request.getParameter("id"))));
        HttpSession hs = request.getSession();

        if (hs.getAttribute("current_user") != null) {
            root.put("current_user", hs.getAttribute("current_user"));
        }
        id = Integer.parseInt(request.getParameter("id"));
        root.put("rating",RatingsRepository.ratingById(id));
        root.put("id",id);
        try {
            tmp.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
