package ru.kpfu.itis.NAIL_ALISHEV.servlets;

import ru.kpfu.itis.NAIL_ALISHEV.repository.PlacesRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nail Alishev
 *         11-401
 */
@WebServlet(name = "Suggest")
public class Suggest extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("placeName");
        String category = request.getParameter("category");
        String description = request.getParameter("description");
        HashMap<String, String> additions = getAdditions(request);
        if (isValid(name, category, description)) {
            PlacesRepository.newPlace(name, description, category, additions);
        }
        response.sendRedirect("/welcome");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/Suggest.jsp").forward(request, response);
    }


    private boolean isValid(String name, String category, String description) {
        if (name.equals("") || category.equals("") || description.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    private HashMap<String, String> getAdditions(HttpServletRequest request) {
        HashMap<String, String> additions = new HashMap<>();
        additions.put("address",request.getParameter("address"));
        additions.put("phone",request.getParameter("phone"));
        additions.put("site",request.getParameter("site"));
        return additions;
    }
}