package ru.kpfu.itis.NAIL_ALISHEV.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.NAIL_ALISHEV.repository.PlacesRepository;
import ru.kpfu.itis.NAIL_ALISHEV.repository.RatingsRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by neil on 11.11.15.
 */
@WebServlet(name = "TopTrending")
public class TopTrending extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        HashMap<String, Object> root = new HashMap<>();
        if (request.getParameter("category") == null) {
            root.put("places", RatingsRepository.PlacesByRating("all"));
        } else {
            root.put("places", RatingsRepository.PlacesByRating("category"));
        }

        HttpSession hs = request.getSession();
        if (hs.getAttribute("current_user") != null) {
            root.put("current_user", hs.getAttribute("current_user"));
        }
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmp = cfg.getTemplate("TopTrending.ftl");
        try {
            tmp.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
