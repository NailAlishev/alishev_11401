package ru.kpfu.itis.NAIL_ALISHEV.servlets;

import ru.kpfu.itis.NAIL_ALISHEV.repository.UsersRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Nail Alishev
 * 11-401
 */
@WebServlet(name = "Registration")
public class Registration extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession hs = request.getSession();
        if (hs.getAttribute("current_user") != null) {
            response.sendRedirect("/profile");
        } else {
            if ((!request.getParameter("email").equals("")) && (!request.getParameter("password").equals(""))) {
                UsersRepository.newUser(request.getParameter("email"), request.getParameter("password"),request.getParameter("loginName"));
                hs.setAttribute("current_user", request.getParameter("email"));  // засунуть это в метод auth()
                response.sendRedirect("/welcome");
            } else {
                response.sendRedirect("/login?errorMessage=Invalid input");
            }


        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {
            getServletContext().getRequestDispatcher("/Registration.jsp").forward(request, response);
        }
    }
}