package ru.kpfu.itis.NAIL_ALISHEV.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.NAIL_ALISHEV.repository.BookmarksRepository;
import ru.kpfu.itis.NAIL_ALISHEV.repository.UsersRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * @author Nail Alishev
 *         11-401
 */
@WebServlet(name = "Profile")
public class Profile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        HashMap<String, Object> root = new HashMap<>();
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmp = cfg.getTemplate("Profile.ftl");
        HttpSession hs = request.getSession();
        root.put("name", UsersRepository.getNameByEmail((String) hs.getAttribute("current_user")));
        if (hs.getAttribute("current_user") != null) {
            root.put("current_user", hs.getAttribute("current_user"));
        }
        root.put("bookmarks", BookmarksRepository.getBookmarksByEmail((String) hs.getAttribute("current_user")));
        try {
            tmp.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
