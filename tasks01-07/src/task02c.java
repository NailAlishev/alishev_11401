/**
 * @author Nail Alishev
 *         11401
 *         task 02c
 */
public class task02c {
    public static void main(String[] args) {
        String[] binaryStrings = {"00000", "1111", "101", "01010", "0010", "1010", "01011"};
        System.out.println("Номера подходящих строк:");
        for (int i = 1; i <= binaryStrings.length; i++) {
            if (binaryStrings[i - 1].matches("(0*)|(1*)|((((01)*)0?)|(((10)*)1?))")) {
                System.out.println(i);
            }
        }
    }
}
