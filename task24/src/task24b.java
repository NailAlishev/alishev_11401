import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nail Alishev
 *         11-401
 *         task 14b
 */
public class task24b {
    private static Map<String, String> urlsAndNames = new HashMap<>();

    public static void main(String[] args) throws Exception {
        URL url = new URL("https://ru.wikipedia.org/wiki/Россия");
        Pattern pattern = Pattern.compile("<.*href=\"(https?://.+/(.+\\.pdf))\".*>");
        Matcher matcher;
        BufferedReader r = new BufferedReader(
                new InputStreamReader(url.openStream()));
        String s = r.readLine();
        matcher = pattern.matcher(s);
        while (s != null) {
            matcher.reset(s);
            while (matcher.find()) {
                urlsAndNames.put(matcher.group(1), matcher.group(2));
            }
            s = r.readLine();
        }
        write();
        r.close();
    }

    private static void write() throws IOException {
        URL url1;
        InputStream is;
        BufferedOutputStream bos;
        byte[] buffer = new byte[256];
        for (String url : urlsAndNames.keySet()) {
            url1 = new URL(url);
            HttpURLConnection httpcon = (HttpURLConnection) url1.openConnection();
            httpcon.addRequestProperty("User-Agent", "Mozilla/4.0");
            try {
                is = new BufferedInputStream(httpcon.getInputStream());
            } catch (FileNotFoundException | UnknownHostException e) {
                continue;
            }
            bos = new BufferedOutputStream(new FileOutputStream("Output" + File.separator + urlsAndNames.get(url)));

            int x = is.read(buffer);
            while (x > 0) {
                bos.write(buffer, 0, x);
                x = is.read(buffer);
            }
            is.close();
            bos.close();
        }
    }
}
