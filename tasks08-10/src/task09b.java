import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Scanner;

/**
 * @author Nail Alishev
 *         11401
 *         task 09b
 */
public class task09b {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        boolean running = true;
        File file = new File("task09b_Output");
        PrintWriter pw;
        String s;
        System.out.println("to exit the program type \"exit\"");
        while (running) {
            s = scanner.nextLine();
            if (s.equals("/getdate")) {
                pw = new PrintWriter(file.getAbsolutePath() + "/" + new Date() + ".html");
                pw.println("<html>");
                pw.println(new Date());
                pw.println("</html>");
                pw.close();
            } else if (s.matches("/(add|mult)/\\d+/\\d+")) {
                Scanner scanner2 = new Scanner(s);
                scanner2.useDelimiter("/");
                pw = new PrintWriter(file.getAbsolutePath() + "/" + new Date() + ".html");
                pw.println("<html>");
                if (scanner2.next().equals("add")) {
                    pw.println(Integer.parseInt(scanner2.next()) + Integer.parseInt(scanner2.next()));
                } else {
                    pw.println(Integer.parseInt(scanner2.next()) * Integer.parseInt(scanner2.next()));
                }
                pw.println("</html>");
                pw.close();
            } else if (s.matches("/((baidu\\.com)|(bing\\.com)|(yahoo\\.com)|(aol\\.com))/search")) {
                Scanner scanner2 = new Scanner(s);
                scanner2.useDelimiter("/");
                String option = scanner2.next();
                pw = new PrintWriter(file.getAbsolutePath() + "/" + new Date() + ".html");
                pw.println("<html>");
                if (option.equals("baidu.com")) {
                    pw.println("<h2>Baidu</h2>\n" +
                            "    <form action =\"http://www.baidu.com/s\">\n" +
                            "      <input type=\"text\" name=\"wd\">\n" +
                            "      <input type=\"submit\" value=\"search\">\n" +
                            "    </form>");
                } else if (option.equals("bing.com")) {

                    pw.println("<h2>Bing</h2>\n" +
                            "    <form action=\"https://www.bing.com/search\">\n" +
                            "       <input type=\"text\" name=\"q\">\n" +
                            "       <input type=\"submit\" value=\"search\">\n" +
                            "    </form>");
                } else if (option.equals("yahoo.com")) {
                    pw.println("<h2>Yahoo</h2>\n" +
                            "    <form action=\"https://search.yahoo.com/search\">\n" +
                            "      \t<input type=\"text\" name=\"p\">\n" +
                            "      \t<input type=\"submit\" value=\"search\">\n" +
                            "    </form>");
                } else {
                    pw.println(" <h2>Aol</h2>\n" +
                            "    <form action=\"http://search.aol.com/aol/search\">\n" +
                            "        <input type=\"text\" name=\"q\">\n" +
                            "        <input type=\"submit\" value=\"search\">\n" +
                            "    </form>");
                }
                pw.println("</html>");
                pw.close();

            } else if (s.equals("exit")) {
                running = false;
            } else {
                pw = new PrintWriter(file.getAbsolutePath() + "/" + new Date() + ".html");
                pw.println("<h1>404 ERROR. PAGE NOT FOUND</h1>");
                pw.close();
            }
        }

    }
}
