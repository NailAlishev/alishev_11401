import ConfigSingleton.ConfigSingleton;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by neil on 12.10.15.
 */
@WebServlet(name = "Search")
public class Search extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmp = cfg.getTemplate("Search.ftl");
        String site = request.getPathInfo().substring(1);
        HashMap<String, String> root = new HashMap<>();
        switch (site) {
            case "baidu":
                root.put("site", "baidu");
                root.put("action", "http://www.baidu.com/s");
                root.put("name", "wd");
                break;
            case "yahoo":
                root.put("site", "yahoo");
                root.put("action", "https://search.yahoo.com/search");
                root.put("name", "p");
                break;
            case "bing":
                root.put("site", "bing");
                root.put("action", "https://www.bing.com/search");
                root.put("name", "q");
                break;
            case "aol":
                root.put("site", "aol");
                root.put("action", "http://search.aol.com/aol/search");
                root.put("name", "q");
                break;
            default:
                response.sendRedirect("/404");
        }
        try {
            tmp.process(root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
