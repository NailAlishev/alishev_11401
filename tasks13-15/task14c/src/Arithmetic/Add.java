package Arithmetic;

import freemarker.template.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import ConfigSingleton.ConfigSingleton;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Created by neil on 12.10.15.
 */
@WebServlet(name = "Add")
public class Add extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmp = cfg.getTemplate("Add.ftl");
        HashMap<String,String> root = new HashMap<>();
        String pathInfo = request.getPathInfo();
        Scanner scanner = new Scanner(pathInfo);
        scanner.useDelimiter("/");
        if (pathInfo.matches("/\\d+/\\d+")) {
            int answer = scanner.nextInt() + scanner.nextInt();
            root.put("answer",Integer.toString(answer));
        } else{
            response.sendRedirect("/404");
        }
        try {
            tmp.process(root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
