import org.apache.velocity.Template;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Nail Alishev
 *         11401
 *         task15f
 */

public class Result extends VelocityServlet {
    public Template handleRequest (HttpServletRequest request,
                                   HttpServletResponse response, Context context) {

        if (!request.getMethod().equals("GET")) { return null; }

        context.put("count", request.getParameter("count"));

        Template template = null;
        try {
            template = getTemplate("result.vm");
        }
        catch (Exception e) {
            System.out.println("Error " + e);
        }

        return template;
    }
}
