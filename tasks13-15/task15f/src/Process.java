/**
 * @author Nail Alishev
 * 11401
 * task 15f
 */

import org.apache.velocity.Template;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Process extends VelocityServlet {

    public Template handleRequest(HttpServletRequest request,
                                  HttpServletResponse response, Context context) throws IOException {
        if (request.getMethod().equals("POST")) {
            Pattern pattern;
            Matcher matcher;
            int count = 0;
            String text = request.getParameter("text");
            if (request.getParameter("operation").equals("characters")) {
                pattern = Pattern.compile("[^\\s]");
                matcher = pattern.matcher(text);
                while (matcher.find()) {
                    count++;
                }
                response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));


            } else if (request.getParameter("operation").equals("words")) {
                pattern = Pattern.compile("[a-zA-Z]+");
                matcher = pattern.matcher(text);
                while (matcher.find()) {
                    count++;
                }
                response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));

            } else if (request.getParameter("operation").equals("sentences")) {
                pattern = Pattern.compile("[A-Z].+?((\\.)|(!)|(\\?))");
                matcher = pattern.matcher(text);
                while (matcher.find()) {
                    count++;
                }
                response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));
            } else {
                pattern = Pattern.compile("[ ]{2,}");
                matcher = pattern.matcher(text);
                while (matcher.find()) {
                    count++;
                }
                response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));
            }
        }

            Template template = null;
            try {
                template = getTemplate("process.vm");
            } catch (Exception e) {
                System.out.println("Error " + e);
            }
            return template;
    }
}
