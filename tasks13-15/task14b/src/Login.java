import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Created by neil on 11.10.15.
 */
@WebServlet(name = "Login")
public class Login extends HttpServlet {
    private Map<String, String> database;

    public void init() {
        database = new HashMap<>();
        database.put("nailalishev@yahoo.com", "123456");
        database.put("blabla@rambler.ru", "hello");
        database.put("heyhey@gmail.com", "heyhey");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession hs = request.getSession();
        if (hs.getAttribute("current_user") != null) {
            response.sendRedirect("/profile");
        } else {
            if ((!request.getParameter("email").equals("")) && (!request.getParameter("password").equals(""))) {
                if (database.get(request.getParameter("email")) != null && (database.get(request.getParameter("email")).equals(request.getParameter("password")))) {
                    hs.setAttribute("current_user", request.getParameter("email"));
                    if (request.getParameter("rememberMe") != null) {
                        Cookie cookie = new Cookie("current_user", request.getParameter("email"));
                        cookie.setMaxAge(24 * 60 * 60);
                        response.addCookie(cookie);
                    }
                    response.sendRedirect("/profile");

                } else {
                    response.sendRedirect("/login?errorMessage=User doesn't exist&username=" + request.getParameter("email"));
                }
            } else {
                response.sendRedirect("/login?errorMessage=Invalid input&username=" + request.getParameter("email"));
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmp = cfg.getTemplate("Login.ftl");
        HashMap<String, String> root = new HashMap<>();
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {


            if (request.getParameter("errorMessage") != null) {
                root.put("errorMessage", request.getParameter("errorMessage"));
                if (request.getParameter("username").matches("\\w+@\\w+\\.\\w+")) {
                    root.put("userName", request.getParameter("username"));
                }
            }

            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals("current_user")) {
                        session.setAttribute("current_user", cookie.getValue());
                        response.sendRedirect("/login");
                    }
                }
            }


            try {
                tmp.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }

        }
    }
}