<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 11.10.15
  Time: 16:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="javax.servlet.ServletException,
                 javax.servlet.annotation.WebServlet,
                 javax.servlet.http.*,
                 java.io.IOException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<%HttpSession session1 = request.getSession();
session1.removeAttribute("current_user");
for (Cookie cookie : request.getCookies()) {
cookie.setMaxAge(0);
response.addCookie(cookie);
}
response.sendRedirect("/login");

%>
</body>
</html>
