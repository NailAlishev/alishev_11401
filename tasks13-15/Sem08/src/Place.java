/**
 */
public class Place {
    private String header;
    private String description;
    private String category;

    public Place(String header, String description, String category) {
        this.header = header;
        this.description = description;
        this.category = category;
    }


    public String getHeader() {
        return header;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }
}