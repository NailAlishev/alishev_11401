import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by neil on 14.10.15.
 */
@WebServlet(name = "WelcomePage")
public class WelcomePage extends HttpServlet {
    private static final HashMap<String, Object> database = new HashMap<>();

    static {
        ArrayList<Place> places = new ArrayList<>();
        places.add(new Place("Это ресторан", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse consectetur libero vitae purus imperdiet, ut malesuada turpis fermentum. Nunc sed enim ut sem malesuada posuere nec ac quam. Ut sed maximus risus, id consectetur quam. Morbi mollis iaculis urna, eu aliquet purus vehicula eget. Phasellus vel orci nibh. Suspendisse eget condimentum dolor, et bibendum dui. Etiam rhoncus lobortis vulputate.", "restaurant"));
        places.add(new Place("Это кафе", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse consectetur libero vitae purus imperdiet, ut malesuada turpis fermentum. Nunc sed enim ut sem malesuada posuere nec ac quam. Ut sed maximus risus, id consectetur quam. Morbi mollis iaculis urna, eu aliquet purus vehicula eget. Phasellus vel orci nibh. Suspendisse eget condimentum dolor, et bibendum dui. Etiam rhoncus lobortis vulputate.", "cafe"));
        places.add(new Place("Это какое-нибудь другое заведение", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse consectetur libero vitae purus imperdiet, ut malesuada turpis fermentum. Nunc sed enim ut sem malesuada posuere nec ac quam. Ut sed maximus risus, id consectetur quam. Morbi mollis iaculis urna, eu aliquet purus vehicula eget. Phasellus vel orci nibh. Suspendisse eget condimentum dolor, et bibendum dui. Etiam rhoncus lobortis vulputate.", "other"));
        places.add(new Place("Это другой ресторан","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus mattis sem posuere ornare varius. Etiam quis ornare mauris, nec iaculis dolor. Sed eget congue dui. Integer non lectus sed justo accumsan consectetur ac sed ex. Aenean dapibus ligula non nisi porta vehicula. Sed et ante eu urna venenatis volutpat in at libero. Donec a fermentum tortor. Nullam vitae pellentesque nisl. Quisque placerat, dolor et luctus euismod, odio felis venenatis diam, in aliquam justo libero quis lorem.","restaurant"));
        database.put("places", places);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> root;
        ArrayList<Place> list;
        if (request.getParameter("category") == null) {
            root = database;
        } else if (request.getParameter("category").equals("restaurants")) {
            root = new HashMap<>();
            list = new ArrayList<>((ArrayList<Place>) database.get("places"));
            Iterator<Place> it = list.iterator();
            Place p;
            while (it.hasNext()) {
                p = it.next();
                if (!p.getCategory().equals("restaurant")) {
                    it.remove();
                }
            }
            root.put("places", list);
        } else if (request.getParameter("category").equals("cafes")) {
            root = new HashMap<>();
            list = new ArrayList<>((ArrayList<Place>) database.get("places"));
            Iterator<Place> it = list.iterator();
            Place p;
            while(it.hasNext()){
                p = it.next();
                if (!p.getCategory().equals("cafe")) {
                    it.remove();
                }

            }
            root.put("places", list);
        } else {
            root = new HashMap<>();
            list = new ArrayList<>((ArrayList<Place>) database.get("places"));
            Iterator<Place> it = list.iterator();
            Place p;
            while(it.hasNext()){
                p = it.next();
                if (!p.getCategory().equals("other")) {
                    it.remove();
                }

            }
            root.put("places", list);
        }

        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmp = cfg.getTemplate("WelcomePage.ftl");
        response.setCharacterEncoding("UTF-8");
        try {
            tmp.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}


