<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 11.10.15
  Time: 17:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
</head>
<body>
<%String site = request.getPathInfo().substring(1);%>
<%if (site.equals("baidu")) {%>
<h2>Baidu</h2>
<form action="http://www.baidu.com/s">
    <input type="text" name="wd" />
    <input type="submit" value="search" />
</form>

<%} else if (site.equals("yahoo")) {%>
<h2>Yahoo</h2>
<form action="https://search.yahoo.com/search">
    <input type="text" name="p">
    <input type="submit" value="search">
</form>

<%} else if (site.equals("bing")) {%>
<h2>Bing</h2>

<form action="https://www.bing.com/search">
    <input type="text" name="q">
    <input type="submit" value="search">
</form>

<%} else if (site.equals("aol")) {%>
<h2>Aol</h2>

<form action="http://search.aol.com/aol/search">
    <input type="text" name="q">
    <input type="submit" value="search">
</form>

<%} else {
response.sendRedirect("/404");
}%>

</body>
</html>
