<%--
  Created by IntelliJ IDEA.
  User: neil
  Date: 11.10.15
  Time: 17:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Scanner" %>
<html>
<head>
    <title></title>
</head>
<body>
<%
    String pathInfo = request.getPathInfo();
    Scanner scanner = new Scanner(pathInfo);
    scanner.useDelimiter("/");
    if (pathInfo.matches("/\\d+/\\d+")) {
        int answer = scanner.nextInt() + scanner.nextInt();
%>
The answer is <%=answer%>
<%
    } else {
        response.sendRedirect("/404");
    }
%>

</body>
</html>
