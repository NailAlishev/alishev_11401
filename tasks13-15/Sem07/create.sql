
CREATE DATABASE my_db;


CREATE TABLE IF NOT EXISTS `my_db`.`User` (
  `username` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NULL,
  `password` VARCHAR(50) NOT NULL,
  `create_time` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`userID`) );


CREATE TABLE IF NOT EXISTS `my_db`.`Place` (
  `placeID` INT NOT NULL AUTO_INCREMENT,
  `placeName` VARCHAR(45) NOT NULL,
  `placeDescription` TEXT NOT NULL,
  `placeRating` DOUBLE NULL,
  PRIMARY KEY (`placeID`) )
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `my_db`.`Comment` (
  `commentID` INT NOT NULL AUTO_INCREMENT,
  `create_time` DATETIME NOT NULL,
  `comment_content` TEXT NOT NULL,
  `userID` INT NULL,
  `placeID` INT NULL,
  PRIMARY KEY (`commentID`) ,
  INDEX `userID_idx` (`userID` ASC) ,
  INDEX `placeID_idx` (`placeID` ASC) ,
  CONSTRAINT `userID`
    FOREIGN KEY (`userID`)
    REFERENCES `my_db`.`user` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `placeID`
    FOREIGN KEY (`placeID`)
    REFERENCES `my_db`.`Place` (`placeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `my_db`.`Bookmark` (
  `bookmarkID` INT NOT NULL,
  `userID` INT NULL,
  `placeID` INT NULL,
  PRIMARY KEY (`bookmarkID`) ,
  INDEX `Hello_idx` (`userID` ASC) ,
  INDEX `placeID_idx` (`placeID` ASC) ,
  CONSTRAINT `userID`
    FOREIGN KEY (`userID`)
    REFERENCES `my_db`.`user` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `placeID`
    FOREIGN KEY (`placeID`)
    REFERENCES `my_db`.`Place` (`placeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;