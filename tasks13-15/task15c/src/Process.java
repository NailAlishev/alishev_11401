import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by neil on 12.10.15.
 */
@WebServlet(name = "Process")
public class Process extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Pattern pattern;
        Matcher matcher;
        int count = 0;
        String text = request.getParameter("text");
        if (request.getParameter("operation").equals("characters")) {
            pattern = Pattern.compile("[^\\s]");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));


        } else if (request.getParameter("operation").equals("words")) {
            pattern = Pattern.compile("[a-zA-Z]+");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));

        } else if (request.getParameter("operation").equals("sentences")) {
            pattern = Pattern.compile("[A-Z].+?((\\.)|(!)|(\\?))");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));
        } else {
            pattern = Pattern.compile("[ ]{2,}");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("Process.html").forward(request, response);
    }
}
