import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @author Nail Alishev
 *         11-401
 *         task 21b
 */
class Calculator extends JFrame {
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JButton jButton17;
    private boolean actionPressed = false;
    private double value1;
    private String actionPerformed;

    public Calculator() {
        ArrayList<JButton> buttons = new ArrayList<JButton>();

        jTextField1 = new javax.swing.JTextField();
        buttons.add(jButton1 = new javax.swing.JButton());
        buttons.add(jButton2 = new javax.swing.JButton());
        buttons.add(jButton3 = new javax.swing.JButton());
        buttons.add(jButton4 = new javax.swing.JButton());
        buttons.add(jButton5 = new javax.swing.JButton());
        buttons.add(jButton6 = new javax.swing.JButton());
        buttons.add(jButton7 = new javax.swing.JButton());
        buttons.add(jButton8 = new javax.swing.JButton());
        buttons.add(jButton9 = new javax.swing.JButton());
        buttons.add(jButton10 = new javax.swing.JButton());
        buttons.add(jButton11 = new javax.swing.JButton());
        buttons.add(jButton12 = new javax.swing.JButton());
        buttons.add(jButton13 = new javax.swing.JButton());
        buttons.add(jButton14 = new javax.swing.JButton());
        buttons.add(jButton15 = new javax.swing.JButton());
        buttons.add(jButton16 = new javax.swing.JButton());
        buttons.add(jButton17 = new javax.swing.JButton());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jButton1.setText("1");

        jButton2.setText("4");

        jButton3.setText("7");

        jButton4.setText("8");

        jButton5.setText("5");

        jButton6.setText("2");

        jButton7.setText("9");

        jButton8.setText("6");

        jButton9.setText("3");

        jButton10.setText("0");

        for (int i = 0; i < 10; i++) {
            final JButton button = buttons.get(i);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (jTextField1.getText().equals("")) {
                        jTextField1.setText(button.getText());
                    } else if (!actionPressed) {
                        jTextField1.setText(jTextField1.getText() + "" + button.getText());
                    } else {
                        jTextField1.setText(button.getText());
                    }
                    actionPressed = false;
                }
            });
        }

        jButton13.setText("+");
        jButton14.setText("-");
        jButton15.setText("*");
        jButton16.setText("/");

        for (int i = 12; i < 15; i++) {
            final JButton button = buttons.get(i);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    actionPressed = true;
                    value1 = Double.parseDouble(jTextField1.getText());
                    actionPerformed = button.getText();
                }
            });
        }


        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton12.setText(".");
        jButton12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jTextField1.getText().equals("") && !jTextField1.getText().contains(".")) {
                    jTextField1.setText(jTextField1.getText() + ".");
                }
            }
        });

        jButton11.setText("c");
        jButton11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jTextField1.setText("");
            }
        });


        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });


        jButton17.setText("=");
        jButton17.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(actionPerformed==null){
                    jTextField1.setText("Выберите действие");
                    throw new IllegalStateException();
                }
                jTextField1.setText(countResult(value1, Double.parseDouble(jTextField1.getText())) + "");
                actionPerformed = null;
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(23, 23, 23)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(jButton2)
                                                                        .addComponent(jButton3)
                                                                        .addComponent(jButton1)
                                                                        .addComponent(jButton10))))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                                        .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                        .addComponent(jButton12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                                                .addGap(30, 30, 30)
                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(jButton9)))
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(jButton4)
                                                                                .addGap(30, 30, 30)
                                                                                .addComponent(jButton7))
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(jButton5)
                                                                                .addGap(30, 30, 30)
                                                                                .addComponent(jButton8)))
                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(jButton17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addGap(12, 12, 12))))
                                        .addComponent(jTextField1)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton13)
                                        .addComponent(jButton14)
                                        .addComponent(jButton15)
                                        .addComponent(jButton16)
                                        .addComponent(jButton17))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton7)
                                        .addComponent(jButton4)
                                        .addComponent(jButton3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton8)
                                        .addComponent(jButton5)
                                        .addComponent(jButton2))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton9)
                                        .addComponent(jButton6)
                                        .addComponent(jButton1))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton11)
                                        .addComponent(jButton12)
                                        .addComponent(jButton10))
                                .addContainerGap(15, Short.MAX_VALUE))
        );

        pack();

        setVisible(true);
    }

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private double countResult(double value1, double value2) {
        switch (actionPerformed) {
            case "+":
                return value1 + value2;
            case "-":
                return value1 - value2;
            case "*":
                return value1 * value2;
            case "/":
                return value1 / value2;
        }
        throw new IllegalArgumentException();
    }

}

public class task21b {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
    }
}
