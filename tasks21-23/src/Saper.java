import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;

/**
 * @author Nail Alishev
 *         11-401
 *         tasks:
 *         task 22c
 *         task 22b
 *         task 22a
 *         task 23b
 */
public class Saper extends JFrame {
    private int size;
    private int minesNumber;
    private int buttonSize = 50;
    private int[][] field;
    private int flags = 0;
    ArrayList<JButton> buttons = new ArrayList<>();

    public Saper(int s, int i1) {
        size = s;
        minesNumber = i1;
        setLayout(new GroupLayout(getContentPane()));
        generateField();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                JButton jb = new JButton();
                buttons.add(jb);
                jb.setBounds(j * buttonSize, i * buttonSize, buttonSize, buttonSize);
                jb.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getButton() == MouseEvent.BUTTON3) {
                            JButton jb1 = (JButton) e.getSource();
                            if (jb1.getText().equals("P")) {
                                jb1.setText("");
                                flags--;
                            } else if (((JButton) e.getSource()).isEnabled()) {
                                jb1.setText("P");
                                flags++;
                                int enabled = howManyButtonsAreEnabled();
                                if (enabled == minesNumber && flags == minesNumber) {
                                    showInfo("You won");
                                    restart();
                                }
                            }
                        } else if (e.getButton() == MouseEvent.BUTTON1) {
                            JButton jb1 = (JButton) e.getSource();
                            int x = jb1.getX() / buttonSize;
                            int y = jb1.getY() / buttonSize;
                            if (field[y][x] == 1) {
                                for (JButton button : buttons) {
                                    if (field[button.getY() / buttonSize][button.getX() / buttonSize] == 1) {
                                        button.setText("X");
                                    }
                                    button.setEnabled(false);
                                }
                                showInfo("Game over");
                                restart();

                            } else {
                                int count = countMinesAround(y, x);
                                if (count == 0) {
                                    openAll(jb1);
                                } else {
                                    jb1.setText(count + "");
                                    jb1.setEnabled(false);
                                }
                            }

                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
                add(jb);
            }
        }

        setBounds(0, 0, buttonSize * size + 50, buttonSize * size + 50);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private int countMinesAround(int x, int y) {
        int s = 0;
        for (int i = -1; i <= 1; i++) {
            if (x + i >= 0 && x + i < size) {
                for (int j = -1; j <= 1; j++) {
                    if (y + j >= 0 && y + j < size)
                        s += field[x + i][y + j];
                }
            }
        }
        return s;
    }

    private void generateField() {
        field = new int[size][size];
        Random r = new Random();
        int k = 0;
        while (k < minesNumber) {
            int i = r.nextInt(size);
            int j = r.nextInt(size);
            if (field[i][j] == 0) {
                field[i][j] = 1;
                k++;
            }
        }
    }

    private void openAll(JButton jb) {
        Queue<JButton> zeroBombButtons = new LinkedList<>();
        zeroBombButtons.offer(jb);
        JButton button;
        int mines;
        while (!zeroBombButtons.isEmpty()) {
            button = zeroBombButtons.poll();
            button.setText("");
            button.setEnabled(false);
            for (int i = -1; i <= 1; i++) {
                if ((button.getY() / buttonSize) + i >= 0 && (button.getY() / buttonSize) + i < size)
                    for (int j = -1; j <= 1; j++) {
                        if ((button.getX() / buttonSize) + j >= 0 && (button.getX() / buttonSize) + j < size) {
                            JButton temp = getButtonByPosition(button.getX() + j * buttonSize, button.getY() + i * buttonSize);
                            if (!temp.isEnabled()) {
                                continue;
                            }
                            mines = countMinesAround(temp.getY() / buttonSize, temp.getX() / buttonSize);

                            if (mines == 0) {
                                zeroBombButtons.offer(temp);
                            } else {
                                temp.setText(mines + "");
                                temp.setEnabled(false);
                            }
                        }
                    }
            }
        }
    }

    private JButton getButtonByPosition(int x, int y) {
        for (JButton button : buttons) {
            if (button.getX() == x && button.getY() == y) {
                return button;
            }
        }
        throw new IllegalStateException();
    }

    private void restart() {
        dispose();
        new Saper(size, minesNumber);
    }

    private void showInfo(String msg) {
        JOptionPane.showConfirmDialog(this, msg, msg, JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE);
    }

    private int howManyButtonsAreEnabled(){
        int enabled = 0;
        for(JButton button : buttons){
            if(button.isEnabled()){
                enabled++;
            }
        }
        return enabled;
    }


    public static void main(String[] args) {
        Saper s = new Saper(10, 10);

    }
}