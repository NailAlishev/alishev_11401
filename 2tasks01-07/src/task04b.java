import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nail Alishev
 *         11401
 *         task 04b
 */
public class task04b {
    static Random rand = new Random();

    public static void main(String[] args) {
        System.out.println("Using matches");
        usingMatches();
        System.out.println();
        System.out.println("Using find");
        usingFind();
    }

    public static void usingMatches() {
        int accepted = 0;
        int overall = 0;
        Integer randomNumber;
        while (accepted < 10) {
            randomNumber = rand.nextInt(Integer.MAX_VALUE);
            if (randomNumber.toString().matches("[02468]{4,5}")) {
                System.out.println(randomNumber);
                accepted++;
            }
            overall++;
        }

        System.out.println("Overall: " + overall);
    }

    public static void usingFind() {
        Pattern p = Pattern.compile("^[02468]{4,5}$");
        int accepted = 0;
        int overall = 0;
        Integer randomNumber;
        Matcher m;
        while (accepted < 10) {
            randomNumber = rand.nextInt(Integer.MAX_VALUE);
            m = p.matcher(randomNumber.toString());
            if (m.find()) {
                System.out.println(m.group());
                accepted++;
            }
            overall++;
        }
        System.out.println("Overall: " + overall);

    }
}
