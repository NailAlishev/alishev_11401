import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nail Alishev
 * 11401
 * task 07c
 */
public class task07c {
    public static void main(String[] args) {
        Pattern p = Pattern.compile("[\\?&]((\\w*)=(\\w+))");
        Matcher m = p.matcher("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "<meta http-equiv=\"set-cookie\" content=\"q_like_id=26469668;domain=kpfu.ru;path=/;expires=Thursday, 12-Feb-2099 18:00:00 GMT\" />\n" +
                "</head>\n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "<meta name='yandex-verification' content='4170a4a1562f590d' />\n" +
                "<meta name=\"google-site-verification\" content=\"YKj0-jgwp5_8YKVvgocl2-YiFV2n-59bpN5nYpQ0TDo\" />\n" +
                "<meta name=\"majestic-site-verification\" content=\"MJ12_052ee03e-928d-49e7-b84f-c846ed805e52\">\n" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\" /> \n" +
                "<style type=\"text/css\">\n" +
                "   @font-face {\n" +
                "    font-family: 'PFDinTextCompPro-Regular';\n" +
                "    src: url('/pdf/fonts/banner_fonts/PFDinTextCompPro-Regular.eot');\n" +
                "    src:  url('/pdf/fonts/banner_fonts/PFDinTextCompPro-Regular.woff'), \n" +
                "          url('/pdf/fonts/banner_fonts/PFDinTextCompPro-Regular.ttf'), \n" +
                "          url('/pdf/fonts/banner_fonts/PFDinTextCompPro-Regular.svg');\n" +
                "    font-weight: normal;\n" +
                "    font-style: normal;\n" +
                "}\n" +
                "\n" +
                ".banner_font_more \n" +
                "{ font-family: Arial Regular,Myriad Pro;\n" +
                "  font-size: 14pt;\n" +
                "  left: 410px;\n" +
                "  position: relative;\n" +
                "  z-index: 10;\n" +
                "  width:470px;\n" +
                "  letter-spacing:0;\n" +
                "  line-height:16px;\n" +
                "  color:#ffffff;\n" +
                "  font-weight:normal;\n" +
                "}\n" +
                ".banner_font \n" +
                "{ font-family: PFDinTextCompPro-Regular,Myriad Pro,Arial Regular;\n" +
                "  font-size: 26pt;\n" +
                "  left: 410px;\n" +
                "  position: relative;\n" +
                "  z-index: 10;\n" +
                "  width:470px;\n" +
                "  height:90px;\n" +
                "  letter-spacing:2px;\n" +
                "  line-height:38px;color:#ffffff;\n" +
                "  font-weight:normal;\n" +
                "} \n" +
                ".banner_font_small \n" +
                "{ font-family: PFDinTextCompPro-Regular,Myriad Pro,Arial Regular;\n" +
                "  font-size: 22pt;\n" +
                "  left: 410px;\n" +
                "  position: relative;\n" +
                "  z-index: 10;\n" +
                "  width:470px;\n" +
                "  height:90px;\n" +
                "  letter-spacing:2px;\n" +
                "  line-height:30px;\n" +
                "  color:#ffffff;\n" +
                "  font-weight:normal;\n" +
                "} \n" +
                "#go_top_text {\n" +
                "    background: url(\"http://shelly.kpfu.ru/pdf/images/portal/toplink.gif\") no-repeat scroll left 3px rgba(0, 0, 0, 0);\n" +
                "    color: #45688E;\n" +
                "    display: block;\n" +
                "    font-weight: bold;\n" +
                "    height: 14px;\n" +
                "    margin: 0 31px 0 15px;\n" +
                "    padding: 0 20px;\n" +
                "} \n" +
                "\n" +
                "#n_lang_menu {\n" +
                "    clear: both;\n" +
                "    height: 36px;\n" +
                "    margin: 1px 0 0;\n" +
                "    position: relative;\n" +
                "}\n" +
                "#n_lang_menu ul {\n" +
                "    float: left;\n" +
                "    left: 20px;\n" +
                "    position: absolute;\n" +
                "    top: -15px;\n" +
                "}\n" +
                "\n" +
                "#n_lang_menu ul li {\n" +
                "    display: inline;\n" +
                "    float: left;\n" +
                "    height:26px;\n" +
                "    position: relative;\n" +
                "    background-image:none!important;\n" +
                "    \n" +
                "}\n" +
                "#n_lang_menu ul li a {\n" +
                "    display: block;\n" +
                "    padding: 0;\n" +
                "}\n" +
                "#n_lang_menu ul li a:hover {\n" +
                "    color: #FFFFFF;\n" +
                "}\n" +
                "\n" +
                "#n_lang_menu ul ul {\n" +
                "    background: none repeat scroll 0 0 #ffffff;\n" +
                "    height: auto;\n" +
                "    //left: -9990px;\n" +
                "    overflow: visible;\n" +
                "    padding: 0;\n" +
                "    position: absolute;\n" +
                "    top: 22px;\n" +
                "    width: 240px;\n" +
                "    z-index: 9999;\n" +
                "    border: 1px solid #cccccc;\n" +
                "    display: none;\n" +
                "    left: -225px;\n" +
                "}\n" +
                "#n_lang_menu ul li:hover ul {\n" +
                "    background: none repeat scroll 0 0 #ffffff;\n" +
                "    //display: block;\n" +
                "    //left: -74px;\n" +
                "}\n" +
                "\n" +
                "#n_lang_menu li ul a:hover {\n" +
                "    //background: none repeat scroll 0 0 #000000;\n" +
                "    left: auto;\n" +
                "    right: 0;\n" +
                "}\n" +
                "#n_lang_menu ul ul li {\n" +
                "    background: none repeat scroll 0 0 #ffffff;\n" +
                "    display: block;\n" +
                "    float: none;\n" +
                "    font-size: 13px;\n" +
                "    //height: auto;\n" +
                "    line-height: 1.3em;\n" +
                "    position: relative;\n" +
                "}\n" +
                "#n_lang_menu ul ul li:hover, #n_lang_menu ul ul li.hover {\n" +
                "    background: none repeat scroll 0 0 #000000;\n" +
                "}\n" +
                "#n_lang_menu ul ul li a {\n" +
                "    border-left: medium none;\n" +
                "    height: auto;\n" +
                "    line-height: 1.3em;\n" +
                "    overflow: hidden;\n" +
                "    padding: 5px 3px 3px 13px;\n" +
                "    position: relative;\n" +
                "    text-align: left;\n" +
                "    text-decoration: none;\n" +
                "    width: 150px;\n" +
                "}\n" +
                "#n_lang_menu ul ul li a,#n_lang_menu ul ul li a:link, #n_lang_menu ul ul li a:visited {\n" +
                "    //background: none repeat scroll 0 0 #ffffff;\n" +
                "    color: #000000;\n" +
                "    text-decoration: none;\n" +
                "}\n" +
                "#n_lang_menu ul ul li a:hover, #n_lang_menu ul ul li a:active {\n" +
                "    //background: none repeat scroll 0 0 #000000;\n" +
                "    color: #FFFFFF;\n" +
                "    text-decoration: none;\n" +
                "} \n" +
                "</style>\n" +
                "\n" +
                "<!--[if lt IE 9]>\n" +
                "<style type=\"text/css\">\n" +
                ".banner_font \n" +
                "{ font-family: Myriad Pro,Arial Regular;\n" +
                "  font-size: 22pt;\n" +
                "  left: 410px;\n" +
                "  position: relative;\n" +
                "  z-index: 10;\n" +
                "  width:470px;\n" +
                "  height:90px;\n" +
                "  letter-spacing:-1px;\n" +
                "  line-height:38px;color:#ffffff;\n" +
                "  font-weight:normal; \n" +
                "} \n" +
                ".banner_font_small\n" +
                "{ font-family: Myriad Pro,Arial Regular;\n" +
                "  font-size: 18pt;\n" +
                "  left: 410px;\n" +
                "  position: relative;\n" +
                "  z-index: 10;\n" +
                "  width:470px;\n" +
                "  height:90px;\n" +
                "  letter-spacing:-1px;\n" +
                "  line-height:30px;\n" +
                "  color:#ffffff;\n" +
                "  font-weight:normal; \n" +
                "}       \n" +
                "</style>\n" +
                "<![endif]-->\n" +
                "<title>Казанский (Приволжский) федеральный университет</title>\n" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=Windows-1251\" />\n" +
                "         <meta name=\"keywords\" content=\"Казанский (Приволжский) федеральный университет, Казанский федеральный университет, КФУ, Казанский государственный университет, Казанский университет, КПФУ, КПФУ Казань официальный, КФУ сайт, КГУ сайт, КГУ официальный сайт, междисциплинарные исследования, КФУ официальный сайт\" />\n" +
                "<meta name=\"google-site-verification\" content=\"PtQ77EGxx7cOkJV96rXLbObQHgnrO2TzKJtYRk_13wM\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/js/main_page/main.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/js/main_page/enter.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/css/main_page.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/css/news_page.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/images/webportal/hidden_div_style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/css/search_style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/images/webportal/content_style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/js/easyslider1.7/css/screen.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/css/spoiler.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/jquery-1.7.1.min.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/open_hidden_div_script.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/easyslider1.7/js/easySlider1.7.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/main_page/restriction.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/main_page/enter_into_ias.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/main_page/popup_window.js?6789\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/images/ias_student/script.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/jquery.cookie/jquery.cookie.js\"></script> \n" +
                "<link href=\"http://shelly.kpfu.ru/pdf/jQuery/js/main_page/schedule.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\n" +
                " \n" +
                "   <script type=\"text/javascript\">\n" +
                "// <![CDATA[   \n" +
                "    jQuery.fn.addtocopy = function(usercopytxt) {\n" +
                "    var options = {htmlcopytxt: '<p>More: <a href=\"'+window.location.href+'\">'+window.location.href+'</a></p>', minlen: 25, addcopyfirst: false}\n" +
                "    $.extend(options, usercopytxt);\n" +
                "    var copy_sp = document.createElement('span');\n" +
                "    copy_sp.id = 'ctrlcopy';\n" +
                "    copy_sp.innerHTML = options.htmlcopytxt;\n" +
                "    return this.each(function(){\n" +
                "        $(this).mousedown(function(){$('#ctrlcopy').remove();});\n" +
                "        $(this).mouseup(function(){\n" +
                "            if(window.getSelection){    //good times \n" +
                "                var slcted=window.getSelection();\n" +
                "                var seltxt=slcted.toString();\n" +
                "                if(!seltxt||seltxt.length < options.minlen) return;\n" +
                "                var nslct = slcted.getRangeAt(0);\n" +
                "                seltxt = nslct.cloneRange();\n" +
                "                seltxt.collapse(options.addcopyfirst);\n" +
                "                seltxt.insertNode(copy_sp);\n" +
                "                if (!options.addcopyfirst) nslct.setEndAfter(copy_sp);\n" +
                "                slcted.removeAllRanges();\n" +
                "                slcted.addRange(nslct);\n" +
                "            } else if(document.selection){    //bad times\n" +
                "                var slcted = document.selection;\n" +
                "                var nslct=slcted.createRange();\n" +
                "                var seltxt=nslct.text;\n" +
                "                if (!seltxt||seltxt.length < options.minlen) return;\n" +
                "                seltxt=nslct.duplicate();\n" +
                "                seltxt.collapse(options.addcopyfirst);\n" +
                "                seltxt.pasteHTML(copy_sp.outerHTML);\n" +
                "                if (!options.addcopyfirst) {nslct.setEndPoint(\"EndToEnd\",seltxt); nslct.select();}\n" +
                "            }\n" +
                "        });\n" +
                "  });\n" +
                "}\n" +
                " // ]]>  \n" +
                " </script>\n" +
                "<style type=\"text/css\">       \n" +
                "        #ctrlcopy {\n" +
                "            color:transparent;\n" +
                "            height:1px;\n" +
                "            overflow:hidden;\n" +
                "            position:absolute;\n" +
                "            width:1px;\n" +
                "        }\n" +
                "    </style>  \n" +
                "    <script type=\"text/javascript\">\n" +
                "// <![CDATA[ \n" +
                "        $(function(){\n" +
                "            $(\"#grey_area\").addtocopy({htmlcopytxt: '<p>Подробности: <a href=\"'+window.location.href+'\">'+window.location.href+'</a></p>Любое использование материалов допускается только при наличии гиперссылки на портал КФУ (kpfu.ru)'});\n" +
                "        }); \n" +
                " // ]]>               \n" +
                "    </script> \n" +
                "<script language=\"JavaScript\" type=\"text/javascript\"> \n" +
                "$(function() { \n" +
                "$.fn.scrollToTop = function() { \n" +
                "$(this).hide().removeAttr(\"href\"); \n" +
                "if ($(window).scrollTop() >= \"250\") $(this).fadeIn(\"slow\") \n" +
                "var scrollDiv = $(this); \n" +
                "$(window).scroll(function() { \n" +
                "if ($(window).scrollTop() <= \"250\") $(scrollDiv).fadeOut(\"slow\") \n" +
                "else $(scrollDiv).fadeIn(\"slow\") \n" +
                "}); \n" +
                "$(this).click(function() { \n" +
                "$(\"html, body\").animate({scrollTop: 0}, \"slow\") \n" +
                "}) \n" +
                "} \n" +
                "}); \n" +
                "$(function() { \n" +
                "$(\"#Go_Top\").scrollToTop(); \n" +
                "});\n" +
                "</script>\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/comm_submit.js\"></script>\n" +
                "<style type=\"text/css\">ul li {  padding-bottom: 2px;padding-left:0px; background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/Layer_143.png);background-repeat:no-repeat;background-position: left top;} </style>\n" +
                "\n" +
                "<script type=\"text/javascript\">\n" +
                "    $(document).ready(function (){\n" +
                "        $('a.no_text').click(function (){\n" +
                "            $(this).next('div').toggleClass('on')\n" +
                "        })\n" +
                "    });\n" +
                "</script>\n" +
                "\n" +
                "<script type=\"text/javascript\"> // для позиционирования плавающего слоя AddThis относительно центра, а не левого верхнего края экрана\n" +
                "    $(document).ready(function(){\n" +
                "         var vStyle=\"top:248px;left:20px;visibility:visible;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/back_main.png);\";\n" +
                "         var vWidth_w=$(window).width()/2-530;\n" +
                "            if (vWidth_w>=20) {\n" +
                "                 vStyle=\"top:248px;left:\"+vWidth_w+\"px;visibility:visible;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/back_main.png);\";\n" +
                "            }\n" +
                "         $('#socio_bar').attr({'style': vStyle});      \n" +
                "    });\n" +
                "</script> \n" +
                "<style type=\"text/css\"> \n" +
                "                 #dim_error{ position:absolute; top:0; left:0; width:100%; z-index:100; background: url('http://shelly.kpfu.ru/pdf/picture/mainpage_new/dim.png'); display:none; text-align:left; }\n" +
                "                 </style>\n" +
                "\n" +
                "<script>\n" +
                "  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" +
                "  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" +
                "  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" +
                "  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n" +
                "\n" +
                "  ga('create', 'UA-34242211-1', 'auto');\n" +
                "  ga('send', 'pageview');\n" +
                "\n" +
                "</script>\n" +
                "</head>\n" +
                "<body style=\"margin: 0px 0px;background-image:url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/back_main.png); background-repeat:repeat;\" > \n" +
                "<div class=\"top_block\">\n" +
                "        <div class=\"grey_area unselect\">\n" +
                "<div class=\"tabs top_tabs\" style=\"width:728px;\">\n" +
                "<a href=\"http://kpfu.ru/\" class=\"no_dec top_div_tabs\" style=\"width:100px;margin-right:4px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/Layer_156.png);color:#000;margin-top:2px;padding-top:7px;\">\n" +
                "          Главная</a>\n" +
                "<a href=\"http://kpfu.ru/studentu\" class=\"no_dec top_div_tabs\" style=\"width:100px;margin-right:4px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/Shape_33_1.png);color:white;\">\n" +
                "             <span style=\"margin-top:7px;\">Студенту</span></a>\n" +
                "<a href=\"http://kpfu.ru/admissions\" class=\"no_dec top_div_tabs\" style=\"width:100px;margin-right:4px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/Shape_33_1.png);color:white;\">\n" +
                "             <span style=\"margin-top:7px;\">Абитуриенту</span></a>\n" +
                "<a href=\"http://kpfu.ru/staff\" class=\"no_dec top_div_tabs\" style=\"width:100px;margin-right:4px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/Shape_33_1.png);color:white;\">\n" +
                "             <span style=\"margin-top:7px;\">Сотруднику</span></a>\n" +
                "<a href=\"http://kpfu.ru/alumnus\" class=\"no_dec top_div_tabs\" style=\"width:100px;margin-right:4px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/Shape_33_1.png);color:white;\">\n" +
                "             <span style=\"margin-top:7px;\">Выпускнику</span></a>\n" +
                "<a href=\"http://kpfu.ru/news\" class=\"no_dec top_div_tabs\" style=\"width:100px;margin-right:4px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/Shape_33_1.png);color:white;\">\n" +
                "             <span style=\"margin-top:7px;\">СМИ</span></a>\n" +
                "<a href=\"http://repository.kpfu.ru\" class=\"no_dec top_div_tabs\" style=\"width:100px;margin-right:4px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/repos_but.png);color:white;\">Ресурсы</a>\n" +
                "</div>\n" +
                "<div style=\"float:left;width:257px;margin-top:2px;margin-left: -8px;\">\n" +
                "<div class=\"ya-site-form ya-site-form_inited_no\" onclick=\"return {'bg': '#005DA2', 'target': '_self', 'language': 'ru', 'suggest': true, 'tld': 'ru', 'site_suggest': true, 'action': 'http://kpfu.ru/search', 'webopt': true, 'fontsize': 12, 'arrow': false, 'fg': '#000000', 'searchid': '2060946', 'logo': 'rb', 'websearch': false, 'type': 2}\">\n" +
                "<form action=\"http://yandex.ru/sitesearch\" method=\"get\" target=\"_self\" name=\"search_f\">\n" +
                "<input type=\"hidden\" name=\"searchid\" value=\"2060946\" />\n" +
                "<input type=\"hidden\" name=\"l10n\" value=\"ru\" />\n" +
                "<input type=\"hidden\" name=\"reqenc\" value=\"\" />\n" +
                "<input type=\"text\" name=\"text\" value=\"\" />\n" +
                "<input type=\"submit\" value=\"Найти\" />\n" +
                "</form>\n" +
                "</div><style type=\"text/css\">.ya-page_js_yes .ya-site-form_inited_no { display: none; }  .ya-site-form__search-precise { display: none; } .ya-site-form__search-input {background: none;}  .ya-site-form__input {background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/Layer_209Edit.png); width:205px; height:22px; padding-left: 40px !important;padding-top: 6px !important;}\n" +
                "      #ya-site-form0 .ya-site-form__input-text_type_hint {background-image: none; width:180px;margin-left:30px; border:none;padding-top:3px;}\n" +
                "      #ya-site-form0 .ya-site-form__input-text {background-image: none; width:180px;margin-left:30px; border:none;padding-top:3px;}\n" +
                "      .ya-site-form__search-input-layout-r { display: none; }\n" +
                "      #ya-site-form0 .ya-site-suggest-items li {background-image:none;}</style>\n" +
                "<script type=\"text/javascript\">(function(w,d,c){var s=d.createElement('script'),h=d.getElementsByTagName('script')[0],e=d.documentElement;(' '+e.className+' ').indexOf(' ya-page_js_yes ')===-1&&(e.className+=' ya-page_js_yes');s.type='text/javascript';s.async=true;s.charset='utf-8';s.src=(d.location.protocol==='https:'?'https:':'http:')+'//site.yandex.net/v2.0/js/all.js';h.parentNode.insertBefore(s,h);(w[c]||(w[c]=[])).push(function(){Ya.Site.Form.init()})})(window,document,'yandex_site_callbacks');</script>\n" +
                "</div>\n" +
                "<script type=\"text/javascript\">\n" +
                "function change_ent_type (ttype,tlang){   // еще передаем значение языка ввода\n" +
                "var vproc=\"http://kpfu.ru/enter_ias\";var vparam={p_type:ttype,p_lang:tlang};\n" +
                "$.post(vproc, vparam , function(data) {\n" +
                "        $(\"#dim_content_window\").empty();\n" +
                "        $(\"#dim_content_window\").html(data);\n" +
                "        });    \n" +
                "  }\n" +
                "function open_help() {\n" +
                "if (document.getElementById('vhod_info').style.display=='none') \n" +
                "    document.getElementById('vhod_info').style.display='';\n" +
                "else \n" +
                "    document.getElementById('vhod_info').style.display='none';      \n" +
                "  }  \n" +
                "function usl_enter_ias_reg(e)\n" +
                "{ if(e.keyCode == 13) submit_enter_ias_reg();} \n" +
                "\n" +
                "function usl_enter_ias_reg_other(e)\n" +
                "{ if(e.keyCode == 13) submit_enter_ias_reg_other();} \n" +
                "\n" +
                "function submit_enter_ias_reg_other(tlang)    // передаем значение языка ввода\n" +
                "{var vlength=1;\n" +
                " var v_form=document.form_enter;\n" +
                " var v_text='';\n" +
                " document.form_enter.p_lastname.value=$.trim(document.form_enter.p_lastname.value);\n" +
                " document.form_enter.p_firstname.value=$.trim(document.form_enter.p_firstname.value);\n" +
                " document.form_enter.p_middlename.value=$.trim(document.form_enter.p_middlename.value);\n" +
                " document.form_enter.p_sex.value=$.trim(document.form_enter.p_sex.value);\n" +
                " document.form_enter.p_mail.value=$.trim(document.form_enter.p_mail_login.value);\n" +
                " document.form_enter.p_pass.value=$.trim(document.form_enter.p_pass.value);\n" +
                " document.form_enter.p_pass2.value=$.trim(document.form_enter.p_pass2.value); \n" +
                "\n" +
                "if (tlang==2) {  // для английской версии\n" +
                " if (v_form.p_lastname.value.length==0||v_form.p_firstname.value.length==0||v_form.p_middlename.value.length==0||v_form.p_mail_login.value.length==0||v_form.p_pass.value.length==0||v_form.p_pass2.value.length==0)\n" +
                "    {  v_text='All sections should be completed'; } \n" +
                " else\n" +
                "  if (v_form.p_pass.value.length<6)\n" +
                "    {  v_text='Password can&#39;t be shorter than 6 characters'; } \n" +
                "   else\n" +
                "    if (v_form.p_pass.value!=v_form.p_pass2.value)\n" +
                "       { v_text='Passwords don&#39;t match'; } \n" +
                "     else\n" +
                "       if (!checkmail(v_form.p_mail_login.value))\n" +
                "          {  v_text='Incorrect E-mail'; } \n" +
                "       else\n" +
                "         if (v_form.p_agreement.checked==false)\n" +
                "            { v_text='You have to accept the user agreement for registration'; } \n" +
                "} else { \n" +
                " if (v_form.p_lastname.value.length==0||v_form.p_firstname.value.length==0||v_form.p_middlename.value.length==0||v_form.p_mail_login.value.length==0||v_form.p_pass.value.length==0||v_form.p_pass2.value.length==0)\n" +
                "    { v_text='Все поля обязательны для заполнения';}\n" +
                " else\n" +
                "  if (v_form.p_pass.value.length<6)\n" +
                "    { v_text='Пароль должен быть не короче 6 символов';}\n" +
                "   else\n" +
                "    if (v_form.p_pass.value!=v_form.p_pass2.value)\n" +
                "       { v_text='Введенные пароли не совпадают';} \n" +
                "     else\n" +
                "       if (!checkmail(v_form.p_mail_login.value))\n" +
                "          { v_text='Некорректно введен E-mail';} \n" +
                "       else\n" +
                "         if (v_form.p_agreement.checked==false)\n" +
                "            { v_text='Для регистрации необходимо принять пользовательское соглашение';}  \n" +
                "}\n" +
                "\n" +
                "if (v_text.length==0)\n" +
                "{\n" +
                "$(\"#hint\").css(\"display\",\"none\");\n" +
                "$(\"#dim_window\").fadeOut();\n" +
                "v_form.submit();}\n" +
                "else { \n" +
                "    if (tlang==2) { document.getElementById(\"hint\").innerHTML='Error! '+v_text;    \n" +
                "    } else { document.getElementById(\"hint\").innerHTML='Ошибка! '+v_text;    \n" +
                "    }\n" +
                "  $(\"#hint\").css(\"display\",\"block\");\n" +
                "}\n" +
                "}\n" +
                "\n" +
                "\n" +
                "function submit_enter_ias_reg(tlang)    // передаем значение языка ввода\n" +
                "{var vlength=1;\n" +
                " var v_form=document.form_enter;\n" +
                " var v_text='';\n" +
                " document.form_enter.p_lastname.value=$.trim(document.form_enter.p_lastname.value);\n" +
                " document.form_enter.p_firstname.value=$.trim(document.form_enter.p_firstname.value);\n" +
                " document.form_enter.p_secondname.value=$.trim(document.form_enter.p_secondname.value);\n" +
                " document.form_enter.p_mail.value=$.trim(document.form_enter.p_mail.value);\n" +
                " document.form_enter.p_pass.value=$.trim(document.form_enter.p_pass.value);\n" +
                " document.form_enter.p_pass2.value=$.trim(document.form_enter.p_pass2.value); \n" +
                "\n" +
                "if (tlang==2) {  // для английской версии\n" +
                " if (v_form.p_lastname.value.length==0||v_form.p_firstname.value.length==0||v_form.p_secondname.value.length==0||v_form.p_mail.value.length==0||v_form.p_pass.value.length==0||v_form.p_pass2.value.length==0)\n" +
                "    {  v_text='All sections should be completed'; } \n" +
                " else\n" +
                "  if (v_form.p_pass.value.length<6)\n" +
                "    {  v_text='Password can&#39;t be shorter than 6 characters'; } \n" +
                "   else\n" +
                "    if (v_form.p_pass.value!=v_form.p_pass2.value)\n" +
                "       { v_text='Passwords don&#39;t match'; } \n" +
                "     else\n" +
                "       if (!checkmail(v_form.p_mail.value))\n" +
                "          { v_text='Incorrect E-mail'; } \n" +
                "       else\n" +
                "         if (v_form.p_agreement.checked==false)\n" +
                "            { v_text='You have to accept the user agreement for registration'; } \n" +
                "} else {\n" +
                " if (v_form.p_lastname.value.length==0||v_form.p_firstname.value.length==0||v_form.p_secondname.value.length==0||v_form.p_mail.value.length==0||v_form.p_pass.value.length==0||v_form.p_pass2.value.length==0)\n" +
                "    { v_text='Все поля обязательны для заполнения'; } \n" +
                " else\n" +
                "  if (v_form.p_pass.value.length<6)\n" +
                "    {  v_text='Пароль должен быть не короче 6 символов'; }\n" +
                "   else\n" +
                "    if (v_form.p_pass.value!=v_form.p_pass2.value)\n" +
                "       { v_text='Введенные пароли не совпадают'; }\n" +
                "     else\n" +
                "       if (!checkmail(v_form.p_mail.value))\n" +
                "          {  v_text='Некорректно введен E-mail'; }  \n" +
                "       else\n" +
                "         if (v_form.p_agreement.checked==false)\n" +
                "            { v_text='Для регистрации необходимо принять пользовательское соглашение'; }   \n" +
                "}\n" +
                "\n" +
                "if (v_text.length==0)\n" +
                "{\n" +
                "$(\"#hint\").css(\"display\",\"none\");\n" +
                "$(\"#dim_window\").fadeOut();\n" +
                "v_form.submit();}\n" +
                "else { \n" +
                "    if (tlang==2) { document.getElementById(\"hint\").innerHTML='Error! '+v_text;    \n" +
                "    } else { document.getElementById(\"hint\").innerHTML='Ошибка! '+v_text;    \n" +
                "    }\n" +
                "  $(\"#hint\").css(\"display\",\"block\");\n" +
                "}\n" +
                "}\n" +
                "\n" +
                "\n" +
                "function checkmail(value) {\n" +
                "reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;\n" +
                "if (!value.match(reg)) { return false; }\n" +
                "    else return true;\n" +
                "}\n" +
                "</script>\n" +
                "<div id=\"dim_window\">\n" +
                "          <div id=\"dim_content_window\" style=\"position: absolute;width: 508px;height: 265px;z-index: 200;top: 50%;left: 50%;margin-top: -100px;margin-left: -150px;\">\n" +
                "          </div>\n" +
                "        </div>\n" +
                "</div></div>\n" +
                "<div class=\"grey_area\"  id=\"grey_area\">\n" +
                " \n" +
                "  <div style=\"clear:both;margin: 0px auto;text-align:center; width:962px;height:114px; overflow:hidden;\">     \n" +
                "     <div  style=\"width:250px;margin-top:0px;margin-bottom:0px;padding-bottom:0px;overflow:hidden;float:left;text-align:left;\">\n" +
                "     <a style=\"\" href=\"http://kpfu.ru/\" class=\"no_dec black_link\">\n" +
                "    <img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/kfu_logo.png\" \n" +
                "    alt=\"Казанский (Приволжский) федеральный университет, КФУ\" title=\"Казанский (Приволжский) федеральный университет, КФУ\" style=\"background:none; \" /></a>\n" +
                "    </div>\n" +
                "     <div style=\"float:left; width:462px;padding-top:15px;\"><a title=\"Официальный сайт Казанского (Приволжского) федерального университета, КФУ\" href=\"http://kpfu.ru/\" class=\"no_dec black_link main_title\"><span style=\"font-family: Georgia, Minion Pro;font-size:19pt;\">\n" +
                "    КАЗАНСКИЙ <br /> ФЕДЕРАЛЬНЫЙ УНИВЕРСИТЕТ\n" +
                "     </span>\n" +
                "     </a></div>\n" +
                "    <div style=\" width:250px;text-align:right;float: right;\">\n" +
                "<div id=\"n_lang_menu\" style=\"float:right;clear:both;margin-top:8px;padding-right:20px;width:57px;\">\n" +
                "<img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/rus_flag.png\" style=\"cursor:pointer;\" onclick=\"if(document.getElementById('n_lang_menu_ul_ul').style.display=='none') {document.getElementById('n_lang_menu_ul_ul').style.display='block';} else {document.getElementById('n_lang_menu_ul_ul').style.display='none';}\" /><span style=\"cursor:pointer;\" onclick=\"if(document.getElementById('n_lang_menu_ul_ul').style.display=='none') {document.getElementById('n_lang_menu_ul_ul').style.display='block';} else {document.getElementById('n_lang_menu_ul_ul').style.display='none';}\"> RUS </span>\n" +
                "<ul style=\"cursor:pointer;height:17px;\" onclick=\"if(document.getElementById('n_lang_menu_ul_ul').style.display=='none') {document.getElementById('n_lang_menu_ul_ul').style.display='block';} else {document.getElementById('n_lang_menu_ul_ul').style.display='none';}\"><li><p><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/lang_list.png\"></p><ul id=\"n_lang_menu_ul_ul\" style=\"display:none;\">\n" +
                "<li style=\"float:left;width:80px;\"><a href=\"http://kpfu.ru/eng\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/en_flag.png\"> EN </a></li>\n" +
                "<li style=\"float:left;width:80px;\"><a href=\"http://kpfu.ru/cn\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/cn_flag.png\"> &#20013;&#25991; </a></li>\n" +
                "<li style=\"float:left;width:80px;\"><a href=\"http://kpfu.ru/de\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/de_flag.png\"> DE </a></li>\n" +
                "<li style=\"float:left;width:80px;\"><a href=\"http://kpfu.ru/tr\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/tr_flag.png\"> T&Uuml;RK </a></li>\n" +
                "<li style=\"float:left;width:80px;\"><a href=\"http://kpfu.ru/es\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/es_flag.png\"> ES </a></li>\n" +
                "<li style=\"float:left;width:80px;\"><a href=\"http://kpfu.ru/ar\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/ar_flag.png\"> &#1585;&#1576;&#1610;&#1577; </a></li>\n" +
                "<li style=\"float:left;width:80px;\"><a href=\"http://kpfu.ru/fr\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/fr_flag.png\"> FR </a></li>\n" +
                "</ul></li></ul></div>\n" +
                "<div style=\"width:100%;margin-bottom:10px;margin-top:34px;\" align=\"right\" class=\"div_enter\">\n" +
                " <a title=\"Вход в электронный университет\" class=\"alert1 enter\" >&nbsp;</a>\n" +
                " </div>\n" +
                "<a href=\"http://kpfu.ru/rss\" target=\"_blank\"><img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/rss_round.png\" alt=\"RSS\" title=\"RSS\" border=\"0\"  /></a>\n" +
                "           <a href=\"https://instagram.com/kazanfederaluniversity\" target=\"_blank\"><img style=\"padding-bottom: 3px;padding-right: 2px;\" src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/instagram_icon.png\" alt=\"Ins\" title=\"Instagram\" border=\"0\"  /></a>\n" +
                "     <img src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/Shape_4.gif\" alt=\"\" usemap=\"#planetmap\" border=\"0\" />\n" +
                "     <map name=\"planetmap\" id=\"planetmap\">\n" +
                "     <area shape=\"circle\" coords=\"17,10,15\" alt=\"Вконтакте\" title=\"Вконтакте\" href=\"http://vk.com/kazan_federal_university\" target=\"_blank\"/>\n" +
                "     <area shape=\"circle\" coords=\"55,10,15\" alt=\"twitter\" title=\"twitter\" href=\"https://twitter.com/kpfu1804\" target=\"_blank\"/>\n" +
                "     <area shape=\"circle\" coords=\"91,10,15\" alt=\"facebook\" title=\"facebook\" href=\"http://www.facebook.com/pages/%D0%9A%D0%B0%D0%B7%D0%B0%D0%BD%D1%81%D0%BA%D0%B8%D0%B9-%D0%9F%D1%80%D0%B8%D0%B2%D0%BE%D0%BB%D0%B6%D1%81%D0%BA%D0%B8%D0%B9-%D1%84%D0%B5%D0%B4%D0%B5%D1%80%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9-%D1%83%D0%BD%D0%B8%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%82%D0%B5%D1%82/198430386925321\" target=\"_blank\"/>\n" +
                "     </map>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"area_width\">\n" +
                "<div style=\"margin-left:70px;\" class=\"full_width\">\n" +
                "<div style=\"margin:0 auto;width:962px;\">\n" +
                "<div style=\"background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/sec_menu.png);width:100%;height:39px;margin-bottom:10px;text-align:center;\">\n" +
                "  <div style=\"float:left;width:145px;padding-top:9px;\"><a href=\"http://kpfu.ru/science\" style=\"color:#ffffff;text-decoration:none;\">Наука</a></div>\n" +
                "  <div style=\"float:left;width:145px;padding-top:9px;\"><a href=\"http://kpfu.ru/edu\" style=\"color:#ffffff;text-decoration:none;\">Учеба</a></div>\n" +
                "  <div style=\"float:left;width:445px;padding-top:9px;\"><a href=\"http://kpfu.ru/international\" style=\"color:#ffffff;text-decoration:none;\">Международное и межвузовское сотрудничество</a></div>\n" +
                "  <div style=\"float:left;width:195px;padding-top:9px;\"><a href=\"http://kpfu.ru/social\" style=\"color:#ffffff;text-decoration:none;\">Вне аудиторий</a></div>\n" +
                "</div>\n" +
                "<table style=\"width:962px;clear:both;padding:0px;margin: 0 0 10px 0;\">\n" +
                "        <tr><td align=\"left\" valign=\"top\" style=\"padding:0px;\" colspan=\"2\">\n" +
                "<script type=\"text/javascript\" src=\"http://shelly.kpfu.ru/pdf/jQuery/js/main_page/top_slider_auto.js\"></script>\n" +
                "<style type=\"text/css\">\n" +
                "            #slider1next {\n" +
                "                left: 910px;\n" +
                "            }</style>\n" +
                "\n" +
                "    <div id=\"container\" style=\"overflow:hidden;width:957px;clear:both;height:262px;margin:0; \">\n" +
                "       <div id=\"content\" class=\"overflow\">\n" +
                "         <div id=\"slider\">\n" +
                "         <ul>\n" +
                " \n" +
                "      <li style=\"font-family:Myriad Pro, 'Arial Regular', Arial   sans-serif;width:957px;height:262px;\">\n" +
                "         <div style=\" height:262px;width:957px;\">\n" +
                "<img src=\"/portal/docs/F-942617318/%C1%E0%ED%E5%F0%20%EE%20%CC%E0%EB%EE%EC%20%F3%ED%E8%E2%E5%F0%F1%E8%F2%E5%F2%E5%202015.jpg\" alt=\"Малый университет КФУ объявляет набор слушателей на 2015 - 2016 учебный год\" title=\"Малый университет КФУ объявляет набор слушателей на 2015 - 2016 учебный год\" style=\" height:262px;width:957px;\"  />\n" +
                "</div>\n" +
                "<div  style=\"position:relative;height:262px;width:600px;left:373px;z-index:10;\"> \n" +
                " <a href=\"http://kpfu.ru/news/nabor-v-malyj-universitet-na-2015-ndash-2016.html\" target=\"_blank\" style=\"margin: 0px 50px 20px 35px ;position:relative;top:-65px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/more2.png);width:170px;background-repeat:no-repeat;height:35px;display:block;\"></a>\n" +
                "</div>\n" +
                "<div class=\"banner_font\" style=\"top: -490px;width:430px;left:410px;\">Малый университет КФУ объявляет набор слушателей на 2015 - 2016 учебный год</div>\n" +
                "</li>\n" +
                " \n" +
                "      <li style=\"font-family:Myriad Pro, 'Arial Regular', Arial   sans-serif;width:957px;height:262px;\">\n" +
                "         <div style=\" height:262px;width:957px;\">\n" +
                "<img src=\"/portal/docs/F660936290/8.jpg\" alt=\"Междисциплинарный центр коллективного пользования\" title=\"Междисциплинарный центр коллективного пользования\" style=\" height:262px;width:957px;\"  />\n" +
                "</div>\n" +
                "<div  style=\"position:relative;height:262px;width:600px;left:373px;z-index:10;\"> \n" +
                " <a href=\"http://kpfu.ru/science/centry-kollektivnogo-dostupa/mezhdisciplinarnyj-centr-kollektivnogo-polzovaniya\" target=\"_blank\" style=\"margin: 0px 50px 20px 35px ;position:relative;top:-65px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/more2.png);width:170px;background-repeat:no-repeat;height:35px;display:block;\"></a>\n" +
                "</div>\n" +
                "<div class=\"banner_font\" style=\"top: -490px;width:430px;left:410px;\">Междисциплинарный центр коллективного пользования</div>\n" +
                "</li>\n" +
                " \n" +
                "      <li style=\"font-family:Myriad Pro, 'Arial Regular', Arial   sans-serif;width:957px;height:262px;\">\n" +
                "         <div style=\" height:262px;width:957px;\">\n" +
                "<img src=\"/portal/docs/F273202480/%EB%E8%F6%E5%E8.jpg\" alt=\"Прием заявлений на курсы подготовки к ЕГЭ и ОГЭ (ГИА)\" title=\"Прием заявлений на курсы подготовки к ЕГЭ и ОГЭ (ГИА)\" style=\" height:262px;width:957px;\"  />\n" +
                "</div>\n" +
                "<div  style=\"position:relative;height:262px;width:600px;left:373px;z-index:10;\"> \n" +
                " <a href=\"http://kpfu.ru/priem/centr-testirovaniya-i-podgotovki-k-ege\" target=\"_blank\" style=\"margin: 0px 50px 20px 35px ;position:relative;top:-65px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/more2.png);width:170px;background-repeat:no-repeat;height:35px;display:block;\"></a>\n" +
                "</div>\n" +
                "<div class=\"banner_font\" style=\"top: -490px;width:430px;left:410px;\">Прием заявлений на курсы подготовки к ЕГЭ и ОГЭ (ГИА)</div>\n" +
                "</li>\n" +
                " \n" +
                "      <li style=\"font-family:Myriad Pro, 'Arial Regular', Arial   sans-serif;width:957px;height:262px;\">\n" +
                "         <div style=\" height:262px;width:957px;\">\n" +
                "<img src=\"/portal/docs/F810780807/%F3%EC%ED%E8%EA2.jpg\" alt=\"Конкурс по программе &#39;УМНИК&#39;\" title=\"Конкурс по программе &#39;УМНИК&#39;\" style=\" height:262px;width:957px;\"  />\n" +
                "</div>\n" +
                "<div  style=\"position:relative;height:262px;width:600px;left:373px;z-index:10;\"> \n" +
                " <a href=\"http://kpfu.ru/news/novosti-i-obyavleniya/otkryt-priem-zayavok-ot-studentov-aspirantov-i.html\" target=\"_blank\" style=\"margin: 0px 50px 20px 35px ;position:relative;top:-65px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/more2.png);width:170px;background-repeat:no-repeat;height:35px;display:block;\"></a>\n" +
                "</div>\n" +
                "<div class=\"banner_font\" style=\"top: -490px;width:430px;left:410px;\">Конкурс по программе &#39;УМНИК&#39;</div>\n" +
                "<div class=\"banner_font_more\" style=\"top: -451px;width:430px;left:410px;\">с 20 июля по 31 октября</div>\n" +
                "</li>\n" +
                " \n" +
                "      <li style=\"font-family:Myriad Pro, 'Arial Regular', Arial   sans-serif;width:957px;height:262px;\">\n" +
                "         <div style=\" height:262px;width:957px;\">\n" +
                "<img src=\"/portal/docs/F2057823701/5-100.jpg\" alt=\"Программа повышения конкурентоспособности КФУ\" title=\"Программа повышения конкурентоспособности КФУ\" style=\" height:262px;width:957px;\"  />\n" +
                "</div>\n" +
                "<div  style=\"position:relative;height:262px;width:600px;left:373px;z-index:10;\"> \n" +
                " <a href=\"http://kpfu.ru/cpr/ppk\" target=\"_blank\" style=\"margin: 0px 50px 20px 35px ;position:relative;top:-65px;background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/more2.png);width:170px;background-repeat:no-repeat;height:35px;display:block;\"></a>\n" +
                "</div>\n" +
                "<div class=\"banner_font\" style=\"top: -490px;width:430px;left:410px;\">Программа повышения конкурентоспособности КФУ</div>\n" +
                "</li>\n" +
                "</ul>\n" +
                "            </div> \n" +
                "        </div>\n" +
                "</div> \n" +
                "</td></tr>\n" +
                "<tr><td><table><tr><td align=\"left\" valign=\"top\" style=\"padding:5px;\" colspan=\"4\"></td></tr>\n" +
                "        <tr><td align=\"left\" valign=\"top\" style=\"padding:0px;\">\n" +
                "<div style=\"width:231px;float:left;padding-right:8px;padding-top:0px;\" id=\"right_b_33\">\n" +
                "<div style=\"font-weight:mormal; font-size:1.2em; line-height: 1em; margin:0; padding-bottom:2px;overflow-y:auto;overflow-x:hidden;max-height:300px;width:230px;\" >\n" +
                "<div style=\"font-weight:mormal; font-size:16px; line-height: 1em; margin:0; padding-bottom:2px;\" ><b><a style=\"color:black;text-decoration:none;\" href=http://kpfu.ru/main_page?p_sub=7&p_id=1#event_type>Календарь событий</b></div>\n" +
                "<div style=\"background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/event_header.png); background-repeat:no-repeat; padding: 2px 0 2px 8px;margin-top:10px;\">\n" +
                "            <span style=\"color: white;font-family: Arial Narrow,Arial,Tahoma;font-size: 11pt;\"><b>21.09 Понедельник</b></span></div>\n" +
                "<div style=\"font-family: Arial Narrow,Arial,Tahoma;font-size: 10pt;padding-top:4px;\"><img style=\"padding-bottom:2px;\" src=\"http://shelly.kpfu.ru/pdf/picture/mainpage_new/top_buttons_new/exent_li.png\" /><span style=\"padding-left:7px;\"></span>\n" +
                "           <a  style=\"padding-left:3px;color:black;text-decoration:none;\" href=\"main_page?p_sub=7&p_id=2&p_data=21.09.2015\">Визит делегации Университета Регенсбурга (21-22 сентября)</a></div>\n" +
                "<p style=\"margin-top:10px;float:right;clear:both;font-family: Arial,Tahoma,sans-serif;font-size: 14px;\">\n" +
                "        <a href=\"http://kpfu.ru/main_page?p_sub=7&p_id=1\">Все события</a>\n" +
                "      </p>\n" +
                "</div>\n" +
                "<div style=\"background-image: url(http://shelly.kpfu.ru/pdf/picture/mainpage_new/Layer142-1.png); background-repeat:repeat-x; padding: 5px 0 5px 0;margin-top:10px;\"></div>\n" +
                "<div style=\"max-height:500px; width:230px;overflow-y:auto;overflow-x:hidden;\">\n" +
                "<div style=\"font-weight:mormal; font-size:16px; line-height: 1em; margin:0; padding-bottom:2px;padding-left:2px;\" ><b><a style=\"color:black;text-decoration:none;\" href=http://kpfu.ru/main_page?p_sub=12&p_type=5>Объявления</b></div>\n" +
                "<script type=\"text/JavaScript\">\n" +
                "if ('5'==2){\n" +
                "           var vcount2='1';\n" +
                "           $(function(){\n" +
                "             $('#more_news2').click(function() {\n" +
                "                var vtag=$('#vtag').val();\n" +
                "                $.post(\"http://kpfu.ru/news_list_content\", {p_sub :  '12', p_width: '215',p_all: '',p_count: vcount2, p_tag_id: vtag, p_ctype: 2} , function(data) {\n" +
                "                     $('#news_list2').append(data);\n" +
                "                     vcount2=++vcount2;\n" +
                "                 });\n" +
                "              });\n" +
                "            });\n" +
                "}\n" +
                "else{\n" +
                "  if ('5'==5)\n" +
                "    {\n" +
                "          var vcount5='1';\n" +
                "          $(function(){\n" +
                "            $('#more_news5').click(function() {\n" +
                "                 var vtag=$('#vtag').val();\n" +
                "                 $.post(\"http://kpfu.ru/news_list_content\", {p_sub :  '12', p_width: '215',p_all: '',p_count: vcount5, p_tag_id: vtag, p_ctype: 5} , function(data) {\n" +
                "                    $('#news_list5').append(data);\n" +
                "                    vcount5=++vcount5;\n" +
                "                  });\n" +
                "             });\n" +
                "          });\n" +
                "     }\n" +
                "  else {\n" +
                "            if ('5'==1)\n" +
                "            {\n" +
                "               var vcount1='1';\n" +
                "               $(function(){\n" +
                "                 $('#more_news1').click(function() {\n" +
                "                    var vtag=$('#vtag').val();\n" +
                "                    $.post(\"http://kpfu.ru/news_list_content\", {p_sub :  '12', p_width: '215',p_all: '',p_count: vcount1, p_tag_id: vtag, p_ctype: 1} , function(data) {\n" +
                "                         $('#news_list1').append(data);\n" +
                "                         vcount1=++vcount1;\n" +
                "                     });\n" +
                "                  });\n" +
                "                });\n" +
                "            }\n" +
                "            else \n" +
                "              {  var vcount='1';\n" +
                "                 $(function(){\n" +
                "                 $('#more_news').click(function() {\n" +
                "                  var vtag=$('#vtag').val();\n" +
                "                  $.post(\"http://kpfu.ru/news_list_content\", {p_sub :  '12', p_width: '215',p_all: '',p_count: vcount, p_tag_id: vtag} , function(data) {\n" +
                "                  $('#news_list').append(data);\n" +
                "                  vcount=++vcount;\n" +
                "                 });\n" +
                "                 });\n" +
                "                  });\n" +
                "               }\n" +
                "        }\n" +
                "}\n" +
                "\n" +
                "</script>\n" +
                "<div id=\"news_list5\">\n" +
                "<input type=\"hidden\" name=\"tag\" id=\"vtag\" value=\"\" />\n" +
                "     </html>\n" +
                "\n");
        while(m.find()){
            System.out.println("Param String: "+m.group(1));
            System.out.println("Name: "+m.group(2));
            System.out.println("Value: "+m.group(3));
            System.out.println();
        }
    }
}
