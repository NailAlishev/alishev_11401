import java.util.Random;

/**
 * @author Nail Alishev
 *         11401
 *         task 03c
 */
public class task03c {
    public static void main(String[] args) {
        Random rand = new Random();
        int accepted = 0;
        int overall = 0;
        Integer randomNumber;
        while (accepted < 10) {
            randomNumber = rand.nextInt(Integer.MAX_VALUE);
            if (randomNumber.toString().matches("\\d*[02468]")) {
                System.out.println(randomNumber);
                accepted++;
            }
            overall++;
        }
        System.out.println(overall);
    }
}

