import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nail Alishev
 *         11401
 *         task 05b
 */
public class task05b {
    static Random rand = new Random();

    public static void main(String[] args) {
        System.out.println("Using matches");
        usingMatches();
        System.out.println();
        System.out.println("Using find");
        usingFind();
    }

    public static void usingMatches() {
        int accepted = 0;
        int overall = 0;
        Integer randomNumber;
        while(accepted<10){
            randomNumber = rand.nextInt(Integer.MAX_VALUE);
            if(randomNumber.toString().matches("\\d*[02468]{2}\\d*[02468]{2}\\d*")){
                System.out.println(randomNumber);
                accepted++;
            }
            overall++;
        }
        System.out.println(overall);
    }

    public static void usingFind() {
        int accepted = 0;
        int overall = 0;
        Integer randomNumber;
        Pattern p = Pattern.compile("\\d*[02468]{2}\\d*[02468]{2}\\d*");
        Matcher m;
        while(accepted<10){
            randomNumber = rand.nextInt(Integer.MAX_VALUE);
            m = p.matcher(randomNumber.toString());
            if(m.find()){
                System.out.println(m.group());
                accepted++;
            }
            overall++;
        }
        System.out.println(overall);
    }
}
