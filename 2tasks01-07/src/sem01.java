/**
 * @author Nail Alishev
 * 11401
 * task sem01
 */
public class sem01 {
    public static void main(String[] args) {
        //адрес
        System.out.println("ул.Кремлевская,1".matches("ул\\.([а-яА-Я]+),\\d+"));
        //логин пользователя
        System.out.println("NailAlishev".matches("\\w{1,20}"));
        //Режим работы
        System.out.println("ПН-ПТ: 12.00 - 02.00".matches("((ПН|ВТ|СР|ЧТ|ПТ|СБ|ВС)-(ПН|ВТ|СР|ЧТ|ПТ|СБ|ВС)): (((0[0-9])|(1[0-9]))\\.[0-5][0-9]) - (((1[8-9])|(2[0-3])|(0[0-4]))\\.[0-5][0-9])"));
    }
}
