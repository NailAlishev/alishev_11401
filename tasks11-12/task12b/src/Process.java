import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by neil on 03.10.15.
 */
@WebServlet(name = "Process")
public class Process extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Pattern pattern;
        Matcher matcher;
        int count = 0;
        String text = request.getParameter("text");
        if (request.getParameter("operation").equals("characters")) {
            pattern = Pattern.compile("[^\\s]");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));


        } else if (request.getParameter("operation").equals("words")) {
            pattern = Pattern.compile("[a-zA-Z]+");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));

        } else if (request.getParameter("operation").equals("sentences")) {
            pattern = Pattern.compile("[A-Z].+?((\\.)|(!)|(\\?))");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));
        } else {
            pattern = Pattern.compile("[ ]{2,}");
            matcher = pattern.matcher(text);
            while (matcher.find()) {
                count++;
            }
            response.sendRedirect("/result?count=" + count + "&operation=" + request.getParameter("operation"));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<form action='/process' method='post'");
        out.println("<p>Enter your text here: </p>");
        out.println("<textarea name='text' cols ='30' rows= '6'>");
        out.println("</textarea>");
        out.println("<p>Choose operation: </p>");
        out.println("<select name='operation'>");
        out.println("<option value='characters'>Number of characters</option>");
        out.println("<option value='words'>Number of words</option>");
        out.println("<option value='sentences'>Number of sentences</option>");
        out.println("<option value='paragraphs'>Number of paragraphs</option>");
        out.println("</select>");
        out.println("<input type='submit' value='Process!' />");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");

    }
}
