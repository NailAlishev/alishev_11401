import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by neil on 03.10.15.
 */
@WebServlet(name = "Profile")
public class Profile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        HttpSession hs = request.getSession();
        if (hs.getAttribute("current_user") == null) {
            response.sendRedirect("/login");
        }

        pw.println("<html>");
        pw.println("Hi, " + hs.getAttribute("current_user") + "!");
        pw.println("<br />");
        pw.println("<a href='/logout'>Log out</a>");
        pw.println("</html>");
    }
}
