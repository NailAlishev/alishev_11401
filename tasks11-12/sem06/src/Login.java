import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by neil on 03.10.15.
 */
@WebServlet(name = "Login")
public class Login extends HttpServlet {
    Map<String, String> database;

    public void init() {
        database = new HashMap<>();
        database.put("nailalishev@yahoo.com", "123456");
        database.put("blabla@rambler.ru", "hello");
        database.put("heyhey@gmail.com", "heyhey");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession hs = request.getSession();
        if (hs.getAttribute("current_user") != null) {
            response.sendRedirect("/profile");
        } else {
            if ((!request.getParameter("email").equals("")) && (!request.getParameter("password").equals(""))) {
                if (database.get(request.getParameter("email")) != null && (database.get(request.getParameter("email")).equals(request.getParameter("password")))) {
                    hs.setAttribute("current_user", request.getParameter("email"));
                    if (request.getParameter("rememberMe") != null) {
                        Cookie cookie = new Cookie("current_user", request.getParameter("email"));
                        cookie.setMaxAge(24 * 60 * 60);
                        response.addCookie(cookie);
                    }
                    response.sendRedirect("/profile");

                } else {
                    response.sendRedirect("/login?errorMessage=User doesn't exist&username=" + request.getParameter("email"));
                }
            } else {
                response.sendRedirect("/login?errorMessage=Invalid input&username=" + request.getParameter("email"));
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");

        if (user != null) {
            response.sendRedirect("/profile");
        } else {


            if (request.getParameter("errorMessage") != null) {
                pw.println("<strong>" + request.getParameter("errorMessage") + "</strong> </br>");
                if (request.getParameter("username").matches("\\w+@\\w+\\.\\w+")) {
                    pw.println("username was: " + request.getParameter("username"));
                }
            }

            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals("current_user")) {
                        session.setAttribute("current_user", cookie.getValue());
                        response.sendRedirect("/login");
                    }
                }
            }


            pw.println("<html>");
            pw.println("<head>");
            pw.println("<meta charset='UTF-8' />");
            pw.println("</head>");
            pw.println("<form action='/login' method='POST'>");
            pw.println("<input type='text' name ='email' />");
            pw.println("<input type='password' name='password' />");
            pw.println("<input type='checkbox' name='rememberMe' value='yes' /> Remember me");
            pw.println("<input type='submit' value='Enter'>");
            pw.println("</form>");
            pw.println("</html>");
        }
    }
}
