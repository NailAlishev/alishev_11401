package Arithmetic;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by neil on 03.10.15.
 */
@WebServlet(name = "Add")
public class Add extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String pathInfo = request.getPathInfo();
        Scanner scanner = new Scanner(pathInfo);
        scanner.useDelimiter("/");
        if (pathInfo.matches("/\\d+/\\d+")) {
            int answer = scanner.nextInt() + scanner.nextInt();
            out.println("<html>");
            out.println("The answer is " + answer);
            out.println("</html>");
        } else{
            response.sendRedirect("/404");
        }

    }
}
