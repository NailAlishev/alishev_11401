import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by neil on 03.10.15.
 */
@WebServlet(name = "Search")
public class Search extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String site = request.getPathInfo().substring(1);
        out.println("<html>");
        if (site.equals("baidu")) {
            out.println("<h2>Baidu</h2>\n" +
                    "<form action =\"http://www.baidu.com/s\">\n" +
                    "<input type=\"text\" name=\"wd\">\n" +
                    "<input type=\"submit\" value=\"search\">\n" +
                    "</form>");
        } else if (site.equals("yahoo")) {
            out.println("<h2>Yahoo</h2>\n" +
                    "<form action=\"https://search.yahoo.com/search\">\n" +
                    "<input type=\"text\" name=\"p\">\n" +
                    "<input type=\"submit\" value=\"search\">\n" +
                    "</form>");
        } else if (site.equals("bing")) {
            out.println("<h2>Bing</h2>\n" +
                    "<form action=\"https://www.bing.com/search\">\n" +
                    "<input type=\"text\" name=\"q\">\n" +
                    "<input type=\"submit\" value=\"search\">\n" +
                    "</form>");
        } else if (site.equals("aol")) {
            out.println("<h2>Aol</h2>\n" +
                    "<form action=\"http://search.aol.com/aol/search\">\n" +
                    "<input type=\"text\" name=\"q\">\n" +
                    "<input type=\"submit\" value=\"search\">\n" +
                    "</form>");
        } else {
            response.sendRedirect("/404");
        }
        out.println("</html>");
    }
}
